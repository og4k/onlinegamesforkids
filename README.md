# onlineGamesForKids
This application was generated using JHipster 5.1.0, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.1.0](https://www.jhipster.tech/documentation-archive/v5.1.0).

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.
2. [Yarn][]: We use Yarn to manage Node dependencies.
   Depending on your system, you can install Yarn either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    yarn install

We use yarn scripts and [Webpack][] as our build system.

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./gradlew
    yarn start

[Yarn][] is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `yarn update` and `yarn install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `yarn help update`.

The `yarn run` command will list all of the scripts available to run for this project.

### Service workers

Service workers are commented by default, to enable them please uncomment the following code.

* The service worker registering script in index.html

```html
<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
        .register('./service-worker.js')
        .then(function() { console.log('Service Worker Registered'); });
    }
</script>
```

Note: workbox creates the respective service worker and dynamically generate the `service-worker.js`

### Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

    yarn add --exact leaflet

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

    yarn add --dev --exact @types/leaflet

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Note: there are still few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].



## Building for production

To optimize the onlineGamesForKids application for production, run:

    ./gradlew -Pprod clean bootWar

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar build/libs/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./gradlew test

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    yarn test



For more information, refer to the [Running tests page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/mysql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./gradlew bootWar -Pprod buildDocker

Then run:

    docker-compose -f src/main/docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

# Online Games for Kids Deployment to AWS 

We can use using at least 3 different approaches including: simple EC2 deployment, ECS cluster deployment and using Elastic Beanstalk service. 
Currently we are going to use ECS service considering relative simplicity and opening possibilities to scale resources if needed.

## Create Docker Image

First of all we need to build existing project in the production environment and create a Docker image locally (remember Docker must be installed and ran locally):

    ./gradlew clean bootWar -Pprod buildDocker

This will clean the project, build a war file using the production profile. Then the war file and docker files will be copied to the build/docker folder and 
a Docker images will be created in a local Docker instance.

The next step is to push the local Docker image to a remote Docker repository. 

    docker tag 0eb71530620c anthonyau1/og4k:prod
 , where `0eb71530620c` - Docker imaged ID (docker images)
 , `anthonyau1` - repository in DockerHub
 , `og4k` - image name
 , `prod` - custom tag name
 
Next we have to log:
 
    docker login --username anthonyau1
    
Then we push our images:

    docker push anthonyau1/og4k
    
## Create Docker Image (AWS Repository)

Once we have to get an AWS Repository authentication credentials to be able to push docker images. 
Retrieve the login command to use to authenticate your Docker client to your registry.

    aws ecr get-login --no-include-email --region us-east-1
    
Copy and paste the result in the command line and press Enter.
After successfully login in we again have to build a docker image by typing:

    ./gradlew clean bootWar -Pprod buildDocker
    
After the build completes, tag your image so you can push the image to this repository:  

    docker tag 0eb71530620c 624313384228.dkr.ecr.us-east-1.amazonaws.com/og4k:prod 
 , where `0eb71530620c` - Docker imaged ID (docker images)
 , `624313384228.dkr.ecr.us-east-1.amazonaws.com` - repository in AWS 
 , `og4k` - image name
 , `prod` - custom tag name
 
Finally, we run the following command to push this image to your newly created AWS repository:
 
    docker push 624313384228.dkr.ecr.us-east-1.amazonaws.com/og4k:prod
    
## Create ECS Cluster (we do it only once) 

Make sure you have install `ecs-cli` tool locally.

Next we need to configure ecs profile:

    ecs-cli configure --cluster og4k --region us-east-1 --default-launch-type EC2 --config-name og4k
    
    ecs-cli configure profile --profile-name og4k --access-key {public_access_key} --secret-key {private_access_key}
    
, where `public_access_key` - public key and `private_access_key` - private key. You create it by going to Account -> My Security Credentials -> Access Keys.

Next we create our cluster:

    ecs-cli up --instance-role og4k --keypair OG4Kcert --instance-type t2.small --security-group sg-0f1b23f4a7df44507 --vpc vpc-dfd907a5 --subnets subnet-09449237,subnet-09449237 --cluster-config og4k
    
, where `keypair` - is your key generated to access EC2 inctances
, `security-group` - group assigned to EC2 instances for controlling open ports
, `vpc-dfd907a5` and `subnets` - vpc ID and subnet IDs - used for controlling ip addresses space used for EC2 instances. You can find them in Services -> 
Networking and Content Delivery -> VPC.

## Create and Run ECS service and task

We have to add our private Docker repository to the EC2 instance. Log in to the instance:

    ssh -i ../OG4Kcert.pem ec2-user@ec2-18-209-249-250.compute-1.amazonaws.com
    
    cd /etc/ecs
    
    sudo su
    
    vi ecs.config 
    
Content should be the following:

    ECS_CLUSTER=og4k
    ECS_ENGINE_AUTH_TYPE=docker
    ECS_ENGINE_AUTH_DATA={"https://index.docker.io/v1/":{"username":"anthonyau1","password":"my pass","email":"my email"}}
    
Then we re-start the service:

    stop ecs
    
    start ecs
   
Then we check that it is working:

    docker inspect ecs-agent | grep ECS_DATADIR    

Next we need to run a service and task which will run our EC2 instance and application (don't forget to assign Elastic IP address to a created EC2 instance):

    ecs-cli compose --project-name og4k --file src/main/docker/app-aws.yml --ecs-params src/main/docker/ecs-params.yml service up --timeout 300 --create-log-groups --cluster-config og4k
    
, make sure `app-aws.yml` has ELASTIC IP ADDRESS for mysql.

!!!Log in as admin and remove User account and change password for admin.

If we you use AWS as a repository then use following commands to stop your service:

    ecs-cli compose --project-name og4k --file src/main/docker/app-aws-repository.yml --ecs-params src/main/docker/ecs-params.yml service down --timeout 300 --cluster-config og4k
    
And to run again:

    ecs-cli compose --project-name og4k --file src/main/docker/app-aws-repository.yml --ecs-params src/main/docker/ecs-params.yml service up --timeout 300 --create-log-groups --cluster-config og4k

## So useful commands to remember

Connect to a EC2 instance:
    
    ssh -i ../OG4Kcert.pem ec2-user@ec2-18-209-249-250.compute-1.amazonaws.com
    
Delete all Docker containers (you can also add `--force` key):
    
    docker rm $(docker ps -a -q)
    
Delete all Docker images (you can also add `--force` key):
    
    docker rmi $(docker images -q)
    
If you want to create a Docker container (`onlineGamesForKids` example):

    docker run -p 8080:8080 -e "SPRING_PROFILES_ACTIVE=prod,swagger" -e "SPRING_DATASOURCE_URL=jdbc:mysql://10.0.1.178:3306/onlinegamesforkids?useUnicode=true&characterEncoding=utf8&useSSL=false" -e "JHIPSTER_SLEEP=1" anthonyau1/og4k:prod

If you want to create a Docker container (`mysql` example):

    docker run -p 3306:3306 --env MYSQL_USER=root --env MYSQL_ALLOW_EMPTY_PASSWORD=yes --env MYSQL_DATABASE=onlinegamesforkids mysql:5.7.20 mysqld --lower_case_table_names=1 --skip-ssl --character_set_server=utf8mb4 --explicit_defaults_for_timestamp

Remove a ECS cluster:

    ecs-cli down --force --cluster-config og4k
    
Create a ECS cluster:

    ecs-cli up --instance-role og4k --keypair OG4Kcert --instance-type t2.small --security-group sg-0f1b23f4a7df44507 --vpc vpc-dfd907a5 --subnets subnet-09449237,subnet-09449237 --cluster-config og4k
    
Create a ECS service:

    ecs-cli compose --project-name og4k --file src/main/docker/app-aws.yml --ecs-params src/main/docker/ecs-params.yml service up --timeout 300 --cluster-config og4k
    
We can also remove it by using `down` instead of `up` or just start/stop using `start` or `stop` commands.

We can check a Docker container logs:

    docker logs --since=1h a67ab7990686
    
To download and install Docker-Compose on a EC2 instance:

    curl -L https://github.com/docker/compose/releases/download/1.20.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    
    chmod +x /usr/local/bin/docker-compose

To clean Docker volumes:

    docker volume prune
    
To configure ecs cluster config and profile config:

    ecs-cli configure --cluster og4k --region us-east-1 --default-launch-type EC2 --config-name og4k
    
    ecs-cli configure profile --profile-name og4k --access-key {public_access_key} --secret-key {private_access_key}
    
To create a backup for a running Docker mysql container:

    docker exec CONTAINER /usr/bin/mysqldump -u root --password=root DATABASE > backup.sql
    
To restore a backup for a running Docker mysql container:

    cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE

Some interesting addresses:

    http://localhost:8080/#/register
    http://localhost:8080/#/reset/request
    http://localhost:8080/#/account/settings
       
    http://localhost:8080/#/admin/user-management
    http://localhost:8080/#/admin/user-management/admin/edit
    http://localhost:8080/#/account/password

[JHipster Homepage and latest documentation]: https://www.jhipster.tech
[JHipster 5.1.0 archive]: https://www.jhipster.tech/documentation-archive/v5.1.0

[Using JHipster in development]: https://www.jhipster.tech/documentation-archive/v5.1.0/development/
[Using Docker and Docker-Compose]: https://www.jhipster.tech/documentation-archive/v5.1.0/docker-compose
[Using JHipster in production]: https://www.jhipster.tech/documentation-archive/v5.1.0/production/
[Running tests page]: https://www.jhipster.tech/documentation-archive/v5.1.0/running-tests/
[Setting up Continuous Integration]: https://www.jhipster.tech/documentation-archive/v5.1.0/setting-up-ci/


[Node.js]: https://nodejs.org/
[Yarn]: https://yarnpkg.org/
[Webpack]: https://webpack.github.io/
[Angular CLI]: https://cli.angular.io/
[BrowserSync]: http://www.browsersync.io/
[Jest]: https://facebook.github.io/jest/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/
[Leaflet]: http://leafletjs.com/
[DefinitelyTyped]: http://definitelytyped.org/
