import './contact.scss';

import { Translate, translate } from 'react-jhipster';
import React from 'react';
import { Form, FormGroup, Button, Input, FormFeedback } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { validateEmail } from 'app/shared/util/regex-utils';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { createEntity, reset } from 'app/modules/contact/contact.reducer';
import ReCAPTCHA from 'react-google-recaptcha';
import { SITE_CAPTCHA_KEY } from 'app/config/constants';

export interface IContactState {
  message: string;
  messageValid: boolean;
  name: string;
  nameValid: boolean;
  email: string;
  emailValid: boolean;
  subject: string;
  subjectValid: boolean;
  captcha: string;
}

export interface IContactProps extends StateProps, DispatchProps {}

export class Contact extends React.Component<IContactProps, IContactState> {
  state: IContactState = {
    message: '',
    messageValid: true,
    name: '',
    nameValid: true,
    email: '',
    emailValid: true,
    subject: '',
    subjectValid: true,
    captcha: null
  };

  captchaRef = null;

  constructor(props, ...args) {
    super(props, ...args);

    this.captchaRef = React.createRef();
  }

  componentDidMount() {
    const { message, name, email, subject } = this.props.entity;
    this.setState({ message, name, email, subject });
  }

  componentDidUpdate() {
    const { updating, updateSuccess } = this.props;

    if (!updating && updateSuccess) {
      this.props.reset();
      this.setState({ message: '', name: '', email: '', subject: '' });
    }
  }

  captchaOnChange = captcha => {
    this.setState({ captcha });
  };

  messageOnChange = event => {
    this.setState({ message: event.target.value });
  };

  nameOnChange = event => {
    this.setState({ name: event.target.value });
  };

  emailOnChange = event => {
    this.setState({ email: event.target.value });
  };

  subjectlOnChange = event => {
    this.setState({ subject: event.target.value });
  };

  create = () => {
    const { messageValid, nameValid, emailValid, subjectValid, message, name, email, subject } = this.state;

    if (messageValid && nameValid && emailValid && subjectValid) {
      this.props.createEntity({ message, name, email, subject });
    }
  };

  submit = () => {
    const { message, name, email, subject } = this.state;

    const messageValid = message && message.trim().length && message.length < 10000;
    const nameValid = name && name.trim().length && name.trim().length < 50;
    const emailValid = email && validateEmail(email);
    const subjectValid = subject && subject.trim().length && subject.trim().length < 300;

    this.setState(
      {
        messageValid,
        nameValid,
        emailValid,
        subjectValid
      },
      this.create
    );
  };

  render() {
    const { messageValid, nameValid, emailValid, subjectValid, message, name, email, subject, captcha } = this.state;
    const { updating } = this.props;

    return (
      <div className="contact">
        <header>
          <div className="logo" />
          <div className="sun" />
        </header>

        <section className="content">
          <div>
            <div className="cloud">
              <h1>
                <Translate contentKey="contact.title">Contact</Translate>
              </h1>
            </div>

            <div className="content-bg">
              <p>
                <Translate contentKey="contact.text">Please contact us</Translate>
              </p>
              <div className="separator" />
              <Form>
                <FormGroup>
                  <Input
                    invalid={!messageValid}
                    type="textarea"
                    value={message}
                    onChange={this.messageOnChange}
                    placeholder={translate('contact.message.placeholder')}
                  />
                  {!messageValid && (
                    <FormFeedback>
                      <Translate contentKey="contact.message.invalid">Message is required and must be less than 10000 characters</Translate>
                    </FormFeedback>
                  )}
                </FormGroup>
                <FormGroup>
                  <Input
                    invalid={!nameValid}
                    type="text"
                    value={name}
                    onChange={this.nameOnChange}
                    placeholder={translate('contact.name.placeholder')}
                  />
                  {!nameValid && (
                    <FormFeedback>
                      <Translate contentKey="contact.name.invalid">Name is required and must be less than 50 characters</Translate>
                    </FormFeedback>
                  )}
                </FormGroup>
                <FormGroup>
                  <Input
                    invalid={!emailValid}
                    type="email"
                    value={email}
                    onChange={this.emailOnChange}
                    placeholder={translate('contact.email.placeholder')}
                  />
                  {!emailValid && (
                    <FormFeedback>
                      <Translate contentKey="contact.email.invalid">Email is required</Translate>
                    </FormFeedback>
                  )}
                </FormGroup>
                <FormGroup>
                  <Input
                    invalid={!subjectValid}
                    type="text"
                    value={subject}
                    onChange={this.subjectlOnChange}
                    placeholder={translate('contact.subject.placeholder')}
                  />
                  {!subjectValid && (
                    <FormFeedback>
                      <Translate contentKey="contact.subject.invalid">Subject is required and must be less than 300 characters</Translate>
                    </FormFeedback>
                  )}
                </FormGroup>
                <Button color="primary" disabled={!captcha || updating} onClick={this.submit}>
                  <FontAwesomeIcon icon="envelope" /> Send Message
                </Button>
              </Form>
              <ReCAPTCHA theme="light" ref={this.captchaRef} sitekey={SITE_CAPTCHA_KEY} onChange={this.captchaOnChange} />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = ({ feedback }: IRootState) => ({
  entity: feedback.entity,
  updating: feedback.updating,
  updateSuccess: feedback.updateSuccess
});

const mapDispatchToProps = {
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);
