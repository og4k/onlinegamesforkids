import { defaultValue, IFeedback } from 'app/shared/model/feedback.model';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import { ICrudPutAction } from 'react-jhipster';
import { cleanEntity } from 'app/shared/util/entity-utils';
import axios from 'axios';

export const ACTION_TYPES = {
  CREATE_FEEDBACK: 'contact/CREATE_FEEDBACK',
  RESET: 'contact/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type FeedbackState = Readonly<typeof initialState>;

// Reducer

export default (state: FeedbackState = initialState, action): FeedbackState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.CREATE_FEEDBACK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.CREATE_FEEDBACK):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.CREATE_FEEDBACK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/feedbacks';

// Actions

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

export const createEntity: ICrudPutAction<IFeedback> = entity => async dispatch =>
  dispatch({
    type: ACTION_TYPES.CREATE_FEEDBACK,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
