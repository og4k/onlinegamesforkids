import './home.scss';

import React from 'react';
import { IRootState } from 'app/shared/reducers';
import { getEntities } from 'app/modules/home/home.reducer';
import { connect } from 'react-redux';
import { Card, CardBody, CardText, UncontrolledTooltip, Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DotsLoading from 'app/shared/loading/dots-loading';
import CardVideo from 'app/shared/card-video/card-video';
// tslint:disable:no-submodule-imports
import { chevronCircleRight } from 'react-icons-kit/fa/';
// tslint:enable:no-submodule-imports

export interface IHomeProps extends StateProps, DispatchProps {}
export interface IHomeState {
  currentLocale: undefined;
  gameButtonOverId: number;
  youtubeButtonOverId: number;
}

export class Home extends React.Component<IHomeProps, IHomeState> {
  state: IHomeState = {
    currentLocale: this.props.currentLocale,
    gameButtonOverId: 0,
    youtubeButtonOverId: 0
  };

  cardVideoComponents: { [guid: string]: CardVideo } = {};

  componentDidMount() {
    if (this.state.currentLocale !== undefined) {
      this.getEntities();
    }
  }

  componentDidUpdate() {
    if (this.state.currentLocale !== this.props.currentLocale) {
      this.setState({ currentLocale: this.props.currentLocale }, () => this.getEntities());
    }
  }

  getEntities = () => {
    const id = this.getCompositionId();

    this.props.getEntities(id, this.state.currentLocale);
  };

  getStyle = (): string => {
    const day = new Date().getDate();

    const remainder = day % 3;
    if (remainder === 1) {
      return 'card-type-1';
    } else if (remainder === 2) {
      return 'card-type-2';
    } else if (!remainder) {
      return 'card-type-3';
    }

    return '';
  };

  getCompositionId = () => {
    const day = new Date().getDate();

    switch (day) {
      case 1:
      case 2:
      case 3:
        return 1;
      case 4:
      case 5:
      case 6:
        return 2;
      case 7:
      case 8:
      case 9:
        return 3;
      case 10:
      case 11:
      case 12:
        return 4;
      case 13:
      case 14:
      case 15:
        return 5;
      case 16:
      case 17:
      case 18:
        return 6;
      case 19:
      case 20:
      case 21:
        return 7;
      case 22:
      case 23:
      case 24:
        return 8;
      case 25:
      case 26:
      case 27:
        return 9;
      case 28:
      case 29:
      case 30:
        return 10;
      default:
        return 1;
    }
  };

  onPlayStarted = (guid: string) => {
    Object.keys(this.cardVideoComponents).forEach(key => {
      if (key !== guid) {
        this.cardVideoComponents[key].stop();
      }
    });
  };

  onComponentDidMount = (guid: string, component: CardVideo) => {
    this.cardVideoComponents[guid] = component;
  };

  onComponentWillUnmount = (guid: string) => {
    delete this.cardVideoComponents[guid];
  };

  playClickHandler = product => () => {
    if (product.url.includes('|')) {
      window.open(`#/game/${product.id}`, '_blank');
    } else {
      window.open(product.url, '_blank');
    }
  };

  watchClickHandler = product => () => {
    window.open(`#/details/${product.details.id}`, '_blank');
  };

  gameMouseEnterHandler = product => () => {
    this.setState({ gameButtonOverId: product.id });
  };

  gameMouseLeftHandler = () => {
    this.setState({ gameButtonOverId: 0 });
  };

  youtubeMouseEnterHandler = product => () => {
    this.setState({ youtubeButtonOverId: product.id });
  };

  youtubeMouseLeftHandler = () => {
    this.setState({ youtubeButtonOverId: 0 });
  };

  render() {
    const { productList, loading } = this.props;
    const { gameButtonOverId, youtubeButtonOverId } = this.state;

    return (
      <div className="home">
        {loading ? (
          <DotsLoading showText delay={3000} />
        ) : (
          <div>
            <div className="content">
              {productList.map((product, i) => (
                <Card key={`entity-${i}`} className={this.getStyle()}>
                  <div className="video-container">
                    <CardVideo
                      product={product}
                      playStarted={this.onPlayStarted}
                      componentDidMount={this.onComponentDidMount}
                      componentWillUnmount={this.onComponentWillUnmount}
                    />
                  </div>
                  <CardBody>
                    <CardText id={`tooltip-${i}-description`} className="text-center">
                      {product.name}
                      <UncontrolledTooltip
                        placement="top"
                        target={`tooltip-${i}-description`}
                        delay={{ show: 500, hide: 250 }}
                        className="description-tooltip"
                      >
                        {product.description}
                      </UncontrolledTooltip>
                    </CardText>
                    <div className="card-buttons">
                      <Button
                        color="primary"
                        onClick={this.playClickHandler(product)}
                        onMouseEnter={this.gameMouseEnterHandler(product)}
                        onMouseLeave={this.gameMouseLeftHandler}
                      >
                        <FontAwesomeIcon icon="gamepad" size="lg" spin={product.id === gameButtonOverId} />
                      </Button>
                      <Button
                        color="danger"
                        onClick={this.watchClickHandler(product)}
                        disabled={!product.details}
                        onMouseEnter={this.youtubeMouseEnterHandler(product)}
                        onMouseLeave={this.youtubeMouseLeftHandler}
                      >
                        <FontAwesomeIcon icon={['fab', 'youtube']} size="lg" spin={product.details && product.id === youtubeButtonOverId} />
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ home, locale }: IRootState) => ({
  productList: home.entities,
  loading: home.loading,
  currentLocale: locale.currentLocale
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
