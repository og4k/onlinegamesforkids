import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import axios from 'axios';
import { IProduct } from 'app/shared/model/product.model';

export const ACTION_TYPES = {
  FETCH_PRODUCT_COMPOSITION: 'home/FETCH_PRODUCT_COMPOSITION'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProduct>
};

export type HomeState = Readonly<typeof initialState>;

// Reducer

export default (state: HomeState = initialState, action): HomeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCT_COMPOSITION):
      return {
        ...state,
        errorMessage: null,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCT_COMPOSITION):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCT_COMPOSITION):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    default:
      return state;
  }
};

const getEntitiesApiUrl = 'api/compositions/';
const getEntitiesApiRuUrl = 'api/composition-rus/';

// Actions

export const getEntities = (id, currentLocale) => {
  const url = currentLocale === undefined || currentLocale.toString() === 'en' ? `${getEntitiesApiUrl}` : `${getEntitiesApiRuUrl}`;
  const requestUrl = `${url + id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCT_COMPOSITION,
    payload: axios.get<IProduct>(`${requestUrl}?cacheBuster=${new Date().getTime()}`)
  };
};
