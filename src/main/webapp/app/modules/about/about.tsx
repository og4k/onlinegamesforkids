import './about.scss';

import { Translate } from 'react-jhipster';
import React from 'react';

const About = () => (
  <div className="about">
    <header>
      <div className="logo" />
      <div className="sun" />
    </header>

    <section className="content">
      <div>
        <div className="cloud">
          <h1>
            <Translate contentKey="about.title">About Us</Translate>
          </h1>
        </div>

        <div className="content-bg">
          <p>
            <Translate contentKey="about.text">About KidsGoat!</Translate>
          </p>
          <div className="separator" />
        </div>
      </div>
    </section>
  </div>
);

export default About;
