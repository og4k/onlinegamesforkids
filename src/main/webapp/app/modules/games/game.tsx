import './game.scss';

import React from 'react';

import { RouteComponentProps } from 'react-router';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';

import { getEntity } from 'app/modules/games/game.reducer';
import { AssetType } from 'app/shared/model/asset-type.model';

export interface IGameProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}
export interface IGameState {
  currentLocale: undefined;
  id: number;
  viewportWidth: number;
}

export class Game extends React.Component<IGameProps, IGameState> {
  state: IGameState = {
    currentLocale: this.props.currentLocale,
    id: +this.props.match.params.id,
    viewportWidth: document.documentElement.clientWidth
  };

  resizeHandler = () => {
    this.setState({ viewportWidth: document.documentElement.clientWidth });
  };

  componentDidMount() {
    if (this.state.currentLocale !== undefined) {
      this.getEntity();
    }

    window.addEventListener('resize', this.resizeHandler);
  }

  componentDidUpdate() {
    const { currentLocale } = this.props;
    const id = +this.props.match.params.id;

    if (currentLocale !== this.state.currentLocale || id !== this.state.id) {
      this.setState({ currentLocale, id }, () => this.getEntity());
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeHandler);
  }

  getEntity = () => {
    const { id, currentLocale } = this.state;
    this.props.getEntity(id, currentLocale);
  };

  render() {
    const defaultRender = <div className="details" />;
    const { game } = this.props;

    if (!game) {
      return defaultRender;
    }

    const image = game.assets.find(a => a.type.id === AssetType.Image);
    const { viewportWidth } = this.state;
    const [url, w, h] = game.url.split('|');

    if (!w || !h) {
      return defaultRender;
    }

    let width = +w;
    let height = +h;

    const maxAllowedWidth = viewportWidth * 0.8;
    if (width > maxAllowedWidth) {
      height = (height * maxAllowedWidth) / width;
      width = maxAllowedWidth;
    }

    return (
      <div className="game">
        <div className="center">
          <div className="information">
            <div className="image">
              <img src={image.location} alt={image.description} />
            </div>
            <div className="game-description">
              <div className="title text-center">{game.name}</div>
              <div className="description">{game.description}</div>
            </div>
          </div>
          <div className="game">
            <iframe width={width} height={height} frameBorder="0" scrolling="no" src={url} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ locale, game }: IRootState) => ({
  currentLocale: locale.currentLocale,
  game: game.entity
});

const mapDispatchToProps = {
  getEntity
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);
