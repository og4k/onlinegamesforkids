import axios from 'axios';
import { IProduct } from 'app/shared/model/product.model';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

export const ACTION_TYPES = {
  FETCH_PRODUCT: 'game/FETCH_PRODUCT'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entity: null as IProduct
};

export type GameState = Readonly<typeof initialState>;

// Reducer

export default (state: GameState = initialState, action): GameState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCT):
      return {
        ...state,
        errorMessage: null,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCT):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    default:
      return state;
  }
};

// Actions

const getEntityApiUrl = 'api/products/eager/';
const getEntityApiRuUrl = 'api/product-rus/eager/';

export const getEntity = (id, currentLocale) => {
  const url = currentLocale === undefined || currentLocale.toString() === 'en' ? `${getEntityApiUrl}` : `${getEntityApiRuUrl}`;
  const requestUrl = `${url}${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCT,
    payload: axios.get<IProduct>(`${requestUrl}?cacheBuster=${new Date().getTime()}`)
  };
};
