import axios from 'axios';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import { IDetails } from 'app/shared/model/details.model';

export const ACTION_TYPES = {
  FETCH_PRODUCT_LIST: 'details/FETCH_PRODUCT_LIST'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entity: null as IDetails
};

export type DetailsState = Readonly<typeof initialState>;

// Reducer

export default (state: DetailsState = initialState, action): DetailsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCT_LIST):
      return {
        ...state,
        errorMessage: null,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCT_LIST):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCT_LIST):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    default:
      return state;
  }
};

// Actions

const getEntityApiUrl = 'api/details/';
const getEntityApiRuUrl = 'api/details-rus/';

export const getEntity = (id, currentLocale) => {
  const url = currentLocale === undefined || currentLocale.toString() === 'en' ? `${getEntityApiUrl}` : `${getEntityApiRuUrl}`;
  const requestUrl = `${url}${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCT_LIST,
    payload: axios.get<IDetails>(`${requestUrl}?cacheBuster=${new Date().getTime()}`)
  };
};
