import React from 'react';
import { Translate } from 'react-jhipster';
import { Row, Col } from 'reactstrap';

const GeneralLinks = () => (
  <Row>
    <Col md="9">
      <h2 className="text-primary">
        <Translate contentKey="generalLinks.title">Welcome to KidsGoat!</Translate>
      </h2>
      <p className="lead text-warning">
        <Translate contentKey="generalLinks.subtitle">Please make sure you provided a correct url!</Translate>
      </p>
      <p className="text-info">
        <Translate contentKey="generalLinks.link.label">You can use the following addresses:</Translate>
      </p>

      <ul className="text-info">
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/1" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.all">All Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/2" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.action">Action Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/3" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.adventure">Adventure Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/4" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.arcade">Arcade Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/5" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.board">Board Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/6" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.card">Card Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/8" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.casual">Casual Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/9" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.educational">Educations Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/10" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.music">Music Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/11" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.puzzle">Puzzle Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/12" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.racing">Racing Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/13" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.rpg">Role Playing Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/category/15" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.sport">Sport Games</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/contacts" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.contacts">Contacts</Translate>
          </a>
        </li>
        <li>
          <a className="text-info" href="https://kidsgoat.com/#/about" rel="noopener noreferrer">
            <Translate contentKey="generalLinks.link.about">About</Translate>
          </a>
        </li>
      </ul>
    </Col>
  </Row>
);

export default GeneralLinks;
