import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { getEntity, updateEntity, createEntity, reset } from './category.reducer';
// tslint:disable-next-line:no-unused-variable
import { keysToValues } from 'app/shared/util/entity-utils';

export interface ICategoryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export interface ICategoryUpdateState {
  isNew: boolean;
  idsproduct: any[];
}

export class CategoryUpdate extends React.Component<ICategoryUpdateProps, ICategoryUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idsproduct: [],
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProducts(0, 1000, 'id');
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { categoryEntity } = this.props;
      const entity = {
        ...categoryEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/category');
  };

  productUpdate = element => {
    let selected = Array.from(element.target.selectedOptions).map((e: any) => e.value);
    if (selected.length === 1 && !selected[0]) {
      selected = [];
    }
    this.setState({
      idsproduct: keysToValues(selected, this.props.products, 'name')
    });
  };

  displayproduct(value: any) {
    if (this.state.idsproduct && this.state.idsproduct.length !== 0) {
      const list = [];
      for (const i in this.state.idsproduct) {
        if (this.state.idsproduct[i]) {
          list.push(this.state.idsproduct[i].name);
        }
      }
      return list;
    }
    if (value.products && value.products.length !== 0) {
      const list = [];
      for (const i in value.products) {
        if (value.products[i]) {
          list.push(value.products[i].name);
        }
      }
      this.setState({
        idsproduct: keysToValues(list, this.props.products, 'name')
      });
      return list;
    }
    return null;
  }

  render() {
    const { categoryEntity, products, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="onlineGamesForKidsApp.category.home.createOrEditLabel">
              <Translate contentKey="onlineGamesForKidsApp.category.home.createOrEditLabel">Create or edit a Category</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : categoryEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="category-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="name">
                    <Translate contentKey="onlineGamesForKidsApp.category.name">Name</Translate>
                  </Label>
                  <AvField
                    id="category-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="onlineGamesForKidsApp.category.description">Description</Translate>
                  </Label>
                  <AvField id="category-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="enabledLabel" check>
                    <AvInput id="category-enabled" type="checkbox" className="form-control" name="enabled" />
                    <Translate contentKey="onlineGamesForKidsApp.category.enabled">Enabled</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="products">
                    <Translate contentKey="onlineGamesForKidsApp.category.product">Product</Translate>
                  </Label>
                  <AvInput
                    id="category-product"
                    type="select"
                    multiple
                    className="form-control"
                    name="fakeproducts"
                    value={this.displayproduct(categoryEntity)}
                    onChange={this.productUpdate}
                  >
                    <option value="" key="0" />
                    {products
                      ? products.map(otherEntity => (
                          <option value={otherEntity.name} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvInput id="category-product" type="hidden" name="products" value={this.state.idsproduct} />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/category" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  products: storeState.product.entities,
  categoryEntity: storeState.category.entity,
  loading: storeState.category.loading,
  updating: storeState.category.updating
});

const mapDispatchToProps = {
  getProducts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryUpdate);
