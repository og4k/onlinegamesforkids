import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProductRu from './product-ru';
import ProductRuDetail from './product-ru-detail';
import ProductRuUpdate from './product-ru-update';
import ProductRuDeleteDialog from './product-ru-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProductRuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProductRuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProductRuDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProductRu} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProductRuDeleteDialog} />
  </>
);

export default Routes;
