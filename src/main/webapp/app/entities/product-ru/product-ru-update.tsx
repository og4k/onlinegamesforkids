import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICategoryRu } from 'app/shared/model/category-ru.model';
import { getEntities as getCategoryRus } from 'app/entities/category-ru/category-ru.reducer';
import { getEntity, updateEntity, createEntity, reset } from './product-ru.reducer';
import { IProductRu } from 'app/shared/model/product-ru.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IProductRuUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export interface IProductRuUpdateState {
  isNew: boolean;
  categoryId: number;
}

export class ProductRuUpdate extends React.Component<IProductRuUpdateProps, IProductRuUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      categoryId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getCategoryRus();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { productRuEntity } = this.props;
      const entity = {
        ...productRuEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/product-ru');
  };

  render() {
    const { productRuEntity, categoryRus, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="onlineGamesForKidsApp.productRu.home.createOrEditLabel">
              <Translate contentKey="onlineGamesForKidsApp.productRu.home.createOrEditLabel">Create or edit a ProductRu</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : productRuEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="product-ru-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="name">
                    <Translate contentKey="onlineGamesForKidsApp.productRu.name">Name</Translate>
                  </Label>
                  <AvField
                    id="product-ru-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="description">
                    <Translate contentKey="onlineGamesForKidsApp.productRu.description">Description</Translate>
                  </Label>
                  <AvField id="product-ru-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="urlLabel" for="url">
                    <Translate contentKey="onlineGamesForKidsApp.productRu.url">Url</Translate>
                  </Label>
                  <AvField
                    id="product-ru-url"
                    type="text"
                    name="url"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 256, errorMessage: translate('entity.validation.maxlength', { max: 256 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="pictureLabel" for="picture">
                    <Translate contentKey="onlineGamesForKidsApp.productRu.picture">Picture</Translate>
                  </Label>
                  <AvField
                    id="product-ru-picture"
                    type="text"
                    name="picture"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 256, errorMessage: translate('entity.validation.maxlength', { max: 256 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="enabledLabel" check>
                    <AvInput id="product-ru-enabled" type="checkbox" className="form-control" name="enabled" />
                    <Translate contentKey="onlineGamesForKidsApp.productRu.enabled">Enabled</Translate>
                  </Label>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/product-ru" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  categoryRus: storeState.categoryRu.entities,
  productRuEntity: storeState.productRu.entity,
  loading: storeState.productRu.loading,
  updating: storeState.productRu.updating
});

const mapDispatchToProps = {
  getCategoryRus,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductRuUpdate);
