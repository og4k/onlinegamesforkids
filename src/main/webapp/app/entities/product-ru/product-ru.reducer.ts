import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProductRu, defaultValue } from 'app/shared/model/product-ru.model';

export const ACTION_TYPES = {
  FETCH_PRODUCTRU_LIST: 'productRu/FETCH_PRODUCTRU_LIST',
  FETCH_PRODUCTRU: 'productRu/FETCH_PRODUCTRU',
  CREATE_PRODUCTRU: 'productRu/CREATE_PRODUCTRU',
  UPDATE_PRODUCTRU: 'productRu/UPDATE_PRODUCTRU',
  DELETE_PRODUCTRU: 'productRu/DELETE_PRODUCTRU',
  RESET: 'productRu/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductRu>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProductRuState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductRuState = initialState, action): ProductRuState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTRU_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTRU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PRODUCTRU):
    case REQUEST(ACTION_TYPES.UPDATE_PRODUCTRU):
    case REQUEST(ACTION_TYPES.DELETE_PRODUCTRU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTRU_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTRU):
    case FAILURE(ACTION_TYPES.CREATE_PRODUCTRU):
    case FAILURE(ACTION_TYPES.UPDATE_PRODUCTRU):
    case FAILURE(ACTION_TYPES.DELETE_PRODUCTRU):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTRU_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTRU):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PRODUCTRU):
    case SUCCESS(ACTION_TYPES.UPDATE_PRODUCTRU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PRODUCTRU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/product-rus';

// Actions

export const getEntities: ICrudGetAllAction<IProductRu> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTRU_LIST,
    payload: axios.get<IProductRu>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProductRu> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTRU,
    payload: axios.get<IProductRu>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProductRu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PRODUCTRU,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProductRu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PRODUCTRU,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProductRu> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PRODUCTRU,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
