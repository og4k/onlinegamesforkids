import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './product-ru.reducer';

export interface IProductRuDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export class ProductRuDeleteDialog extends React.Component<IProductRuDeleteDialogProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  confirmDelete = event => {
    this.props.deleteEntity(this.props.productRuEntity.id);
    this.handleClose(event);
  };

  toggleClicked: (() => void) & ((event: any) => void) = () => {
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { productRuEntity } = this.props;
    return (
      <Modal isOpen toggle={this.toggleClicked}>
        <ModalHeader toggle={this.toggleClicked}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody>
          <Translate contentKey="onlineGamesForKidsApp.productRu.delete.question" interpolate={{ id: productRuEntity.id }}>
            Are you sure you want to delete this ProductRu?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggleClicked}>
            <FontAwesomeIcon icon="ban" />&nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />&nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ productRu }: IRootState) => ({
  productRuEntity: productRu.entity
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductRuDeleteDialog);
