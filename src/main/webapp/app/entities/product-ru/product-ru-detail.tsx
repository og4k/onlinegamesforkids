import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product-ru.reducer';
// tslint:disable-next-line:no-unused-variable

export interface IProductRuDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export class ProductRuDetail extends React.Component<IProductRuDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { productRuEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="onlineGamesForKidsApp.productRu.detail.title">ProductRu</Translate> [<b>{productRuEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="onlineGamesForKidsApp.productRu.name">Name</Translate>
              </span>
            </dt>
            <dd>{productRuEntity.name}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="onlineGamesForKidsApp.productRu.description">Description</Translate>
              </span>
            </dt>
            <dd>{productRuEntity.description}</dd>
            <dt>
              <span id="url">
                <Translate contentKey="onlineGamesForKidsApp.productRu.url">Url</Translate>
              </span>
            </dt>
            <dd>{productRuEntity.url}</dd>
            <dt>
              <span id="enabled">
                <Translate contentKey="onlineGamesForKidsApp.productRu.enabled">Enabled</Translate>
              </span>
            </dt>
            <dd>{productRuEntity.enabled ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/product-ru" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/product-ru/${productRuEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ productRu }: IRootState) => ({
  productRuEntity: productRu.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductRuDetail);
