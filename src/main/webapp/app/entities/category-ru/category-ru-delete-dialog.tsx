import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './category-ru.reducer';

export interface ICategoryRuDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export class CategoryRuDeleteDialog extends React.Component<ICategoryRuDeleteDialogProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  confirmDelete = event => {
    this.props.deleteEntity(this.props.categoryRuEntity.id);
    this.handleClose(event);
  };

  toggleClicked: (() => void) & ((event: any) => void) = () => {
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { categoryRuEntity } = this.props;
    return (
      <Modal isOpen toggle={this.toggleClicked}>
        <ModalHeader toggle={this.toggleClicked}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody>
          <Translate contentKey="onlineGamesForKidsApp.categoryRu.delete.question" interpolate={{ id: categoryRuEntity.id }}>
            Are you sure you want to delete this CategoryRu?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggleClicked}>
            <FontAwesomeIcon icon="ban" />&nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />&nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ categoryRu }: IRootState) => ({
  categoryRuEntity: categoryRu.entity
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryRuDeleteDialog);
