import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import CategoryRu from './category-ru';
import CategoryRuDetail from './category-ru-detail';
import CategoryRuUpdate from './category-ru-update';
import CategoryRuDeleteDialog from './category-ru-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CategoryRuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={CategoryRuUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={CategoryRuDetail} />
      <ErrorBoundaryRoute path={match.url} component={CategoryRu} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={CategoryRuDeleteDialog} />
  </>
);

export default Routes;
