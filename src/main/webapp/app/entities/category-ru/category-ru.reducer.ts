import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICategoryRu, defaultValue } from 'app/shared/model/category-ru.model';

export const ACTION_TYPES = {
  FETCH_CATEGORYRU_LIST: 'categoryRu/FETCH_CATEGORYRU_LIST',
  FETCH_CATEGORYRU: 'categoryRu/FETCH_CATEGORYRU',
  CREATE_CATEGORYRU: 'categoryRu/CREATE_CATEGORYRU',
  UPDATE_CATEGORYRU: 'categoryRu/UPDATE_CATEGORYRU',
  DELETE_CATEGORYRU: 'categoryRu/DELETE_CATEGORYRU',
  RESET: 'categoryRu/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICategoryRu>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type CategoryRuState = Readonly<typeof initialState>;

// Reducer

export default (state: CategoryRuState = initialState, action): CategoryRuState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CATEGORYRU_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CATEGORYRU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CATEGORYRU):
    case REQUEST(ACTION_TYPES.UPDATE_CATEGORYRU):
    case REQUEST(ACTION_TYPES.DELETE_CATEGORYRU):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CATEGORYRU_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CATEGORYRU):
    case FAILURE(ACTION_TYPES.CREATE_CATEGORYRU):
    case FAILURE(ACTION_TYPES.UPDATE_CATEGORYRU):
    case FAILURE(ACTION_TYPES.DELETE_CATEGORYRU):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CATEGORYRU_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CATEGORYRU):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CATEGORYRU):
    case SUCCESS(ACTION_TYPES.UPDATE_CATEGORYRU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CATEGORYRU):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/category-rus';

// Actions

export const getEntities: ICrudGetAllAction<ICategoryRu> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CATEGORYRU_LIST,
    payload: axios.get<ICategoryRu>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICategoryRu> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CATEGORYRU,
    payload: axios.get<ICategoryRu>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICategoryRu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CATEGORYRU,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICategoryRu> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CATEGORYRU,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICategoryRu> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CATEGORYRU,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
