import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './category-ru.reducer';
import { ICategoryRu } from 'app/shared/model/category-ru.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ICategoryRuDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id?: string }> {}

export class CategoryRuDetail extends React.Component<ICategoryRuDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { categoryRuEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="onlineGamesForKidsApp.categoryRu.detail.title">CategoryRu</Translate> [<b>{categoryRuEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="onlineGamesForKidsApp.categoryRu.name">Name</Translate>
              </span>
            </dt>
            <dd>{categoryRuEntity.name}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="onlineGamesForKidsApp.categoryRu.description">Description</Translate>
              </span>
            </dt>
            <dd>{categoryRuEntity.description}</dd>
            <dt>
              <span id="enabled">
                <Translate contentKey="onlineGamesForKidsApp.categoryRu.enabled">Enabled</Translate>
              </span>
            </dt>
            <dd>{categoryRuEntity.enabled ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="onlineGamesForKidsApp.categoryRu.product">Product</Translate>
            </dt>
            <dd>
              {categoryRuEntity.products
                ? categoryRuEntity.products.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.name}</a>
                      {i === categoryRuEntity.products.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/category-ru" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/category-ru/${categoryRuEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ categoryRu }: IRootState) => ({
  categoryRuEntity: categoryRu.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryRuDetail);
