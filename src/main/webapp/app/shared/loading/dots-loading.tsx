import './dots-loading.scss';

import React from 'react';

export interface IDotsLoadingProps {
  delay: number;
  showText: boolean;
}

export interface IDotsLoadingState {
  timeoutToken: number;
  timeIsUp: boolean;
}

export default class DotsLoading extends React.Component<IDotsLoadingProps, IDotsLoadingState> {
  state: IDotsLoadingState = {
    timeoutToken: 0,
    timeIsUp: false
  };

  componentDidMount() {
    const { delay } = this.props;

    if (delay) {
      this.setState({ timeoutToken: setTimeout(() => this.setState({ timeIsUp: true, timeoutToken: 0 }), delay) });
    }
  }

  componentWillUnmount() {
    const { timeoutToken } = this.state;

    if (timeoutToken) {
      window.clearTimeout(timeoutToken);
    }
  }

  render() {
    const { timeIsUp } = this.state;
    const { showText } = this.props;

    return (
      <div className="dots-loading">
        <div className="container">
          {timeIsUp && (
            <div className="loader">
              <div className="loader-dot" />
              <div className="loader-dot" />
              <div className="loader-dot" />
              <div className="loader-dot" />
              <div className="loader-dot" />
              <div className="loader-dot" />
              {showText && <div className="loader-text" />}
            </div>
          )}
        </div>
      </div>
    );
  }
}
