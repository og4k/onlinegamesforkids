import * as React from 'react';
import { JhiPagination } from 'react-jhipster';
import { PaginationItem, PaginationLink } from 'reactstrap';

export class JhiPagination2 extends JhiPagination {
  displayPaginationItem = i => (
    <PaginationItem key={i} active={this.state.currentPage === i + 1}>
      <PaginationLink onClick={this.updateActivePage(i + 1)} href="javascript:void(0)">
        {i + 1}
      </PaginationLink>
    </PaginationItem>
  );
}
