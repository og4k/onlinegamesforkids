import './card-video.scss';

import React from 'react';
import { IProduct } from 'app/shared/model/product.model';
import { CardImg } from 'reactstrap';
import { AssetType } from 'app/shared/model/asset-type.model';
import DotsLoading from 'app/shared/loading/dots-loading';

export interface ICardVideoProps {
  product: IProduct;
  playStarted?: (guid: string) => void;
  componentDidMount?: (guid: string, component: CardVideo) => void;
  componentWillUnmount?: (guid: string, component: CardVideo) => void;
}

export interface ICardVideoState {
  over: boolean;
  loading: boolean;
}

export default class CardVideo extends React.Component<ICardVideoProps, ICardVideoState> {
  state: ICardVideoState = {
    over: false,
    loading: false
  };

  private guid: string;

  s4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);

  componentDidMount() {
    this.guid = (
      this.s4() +
      this.s4() +
      '-' +
      this.s4() +
      '-4' +
      this.s4().substr(0, 3) +
      '-' +
      this.s4() +
      '-' +
      this.s4() +
      this.s4() +
      this.s4()
    ).toLowerCase();

    this.props.componentDidMount(this.guid, this);
  }

  componentWillUnmount() {
    this.props.componentWillUnmount(this.guid, this);
  }

  start() {
    this.setState({ over: true });
  }

  stop() {
    this.setState({ over: false });
  }

  mouseEntered = () => {
    this.props.playStarted(this.guid);

    this.start();
  };

  touchEnd = () => {
    this.props.playStarted(this.guid);

    this.start();
  };

  mouseLeft = () => {
    this.stop();
  };

  blue = () => {
    this.stop();
  };

  loadStart = () => {
    this.setState({ loading: true });
  };

  canPlayThrough = () => {
    this.setState({ loading: false });
  };

  ended = () => {
    this.setState({ over: false, loading: false });
  };

  touchCancel = () => {
    this.setState({ over: false, loading: false });
  };

  render() {
    const { product } = this.props;
    const image = product.assets.find(a => a.type.id === AssetType.Image);
    const video = product.assets.find(a => a.type.id === AssetType.Video);

    const url = product.details ? `#/details/${product.details.id}` : product.url.includes('|') ? `#/game/${product.id}` : product.url;

    return (
      <a
        href={url}
        target="_blank"
        className="card-video"
        onMouseEnter={this.mouseEntered}
        onMouseLeave={this.mouseLeft}
        onTouchEnd={this.touchEnd}
        onTouchCancel={this.touchCancel}
        onBlur={this.blue}
      >
        {!this.state.over ? (
          <div>
            <CardImg bottom src={image.location} alt={image.description} />
          </div>
        ) : (
          <div>
            <video
              autoPlay
              poster={image.location}
              src={video.location}
              onLoadStart={this.loadStart}
              onCanPlayThrough={this.canPlayThrough}
              onEnded={this.ended}
            />
            {this.state.loading && <DotsLoading showText={false} delay={500} />}
          </div>
        )}
      </a>
    );
  }
}
