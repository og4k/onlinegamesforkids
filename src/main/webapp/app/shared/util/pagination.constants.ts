export const TWENTY_ITEMS_PER_PAGE = 20;
export const THIRTY_ITEMS_PER_PAGE = 30;
export const FIFTY_ITEMS_PER_PAGE = 50;
export const HUNDRED_ITEMS_PER_PAGE = 100;
