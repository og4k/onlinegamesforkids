import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import category, {
  CategoryState
} from 'app/entities/category/category.reducer';
// prettier-ignore
import product, {
  ProductState
} from 'app/entities/product/product.reducer';
import menu, { MenuState } from 'app/shared/layout/menu/menu.reducer';
import products, { ProductState as ProductsState } from 'app/products/product.reducer';
// prettier-ignore
import categoryRu, {
  CategoryRuState
} from 'app/entities/category-ru/category-ru.reducer';
// prettier-ignore
import productRu, {
  ProductRuState
} from 'app/entities/product-ru/product-ru.reducer';
import feedback, { FeedbackState } from 'app/modules/contact/contact.reducer';
import home, { HomeState } from 'app/modules/home/home.reducer';
import details, { DetailsState } from 'app/modules/details/details.reducer';
import game, { GameState } from 'app/modules/games/game.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly category: CategoryState;
  readonly product: ProductState;
  readonly menu: MenuState;
  readonly categoryRu: CategoryRuState;
  readonly productRu: ProductRuState;
  readonly products: ProductsState;
  readonly feedback: FeedbackState;
  readonly home: HomeState;
  readonly details: DetailsState;
  readonly game: GameState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  category,
  product,
  menu,
  products,
  categoryRu,
  productRu,
  feedback,
  home,
  details,
  game,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
