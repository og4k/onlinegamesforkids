import './menu.scss';

import React from 'react';
import { getEntities } from 'app/shared/layout/menu/menu.reducer';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { Brand } from 'app/shared/layout/header/header-components';

export interface IMenuProps extends StateProps, DispatchProps {}
export interface IMenuState {
  currentLocale: undefined;
}

export class Menu extends React.Component<IMenuProps, IMenuState> {
  state: IMenuState = {
    currentLocale: this.props.currentLocale
  };

  componentDidMount() {
    if (this.state.currentLocale !== undefined) {
      this.getEntities();
    }
  }

  componentDidUpdate() {
    if (this.state.currentLocale !== this.props.currentLocale) {
      this.setState(
        {
          currentLocale: this.props.currentLocale
        },
        () => this.getEntities()
      );
    }
  }

  getEntities() {
    this.props.getEntities(this.state.currentLocale);
  }

  render() {
    const { menuList, menuOpen } = this.props;

    return (
      <div className={'app-menu' + (!menuOpen ? ' app-menu-closed' : '')}>
        <Brand />
        <Nav vertical>
          {menuList.map((menu, i) => (
            <NavItem key={`entity-${i}`}>
              <NavLink href={`#/category/${menu.id}`}>
                <img src={menu.icon} alt={`${menu.name} icon`} className="nav-link-icon" />
                <div className="nav-link-name">{menu.name}</div>
              </NavLink>
            </NavItem>
          ))}
        </Nav>
      </div>
    );
  }
}

const mapStateToProps = ({ menu, locale }: IRootState) => ({
  menuList: menu.entities,
  currentLocale: locale.currentLocale,
  menuOpen: menu.menuOpen
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);
