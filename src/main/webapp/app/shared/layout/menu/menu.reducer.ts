import { ICategory } from 'app/shared/model/category.model';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import { ICrudGetAllAction } from 'react-jhipster';
import axios from 'axios';
import { IMenu } from 'app/shared/model/menu.model';
import allCategory from '../../../../static/images/categories/all.svg';
import actionCategory from '../../../../static/images/categories/action.svg';
import adventureCategory from '../../../../static/images/categories/adventure.svg';
import arcadeCategory from '../../../../static/images/categories/arcade.svg';
import boardCategory from '../../../../static/images/categories/board.svg';
import cardCategory from '../../../../static/images/categories/card.svg';
import casinoCategory from '../../../../static/images/categories/casino.svg';
import casualCategory from '../../../../static/images/categories/casual.svg';
import educationalCategory from '../../../../static/images/categories/educational.svg';
import musicCategory from '../../../../static/images/categories/music.svg';
import puzzleCategory from '../../../../static/images/categories/puzzle.svg';
import racingCategory from '../../../../static/images/categories/racing.svg';
import rolePlayingCategory from '../../../../static/images/categories/rolePlaying.svg';
import simulationCategory from '../../../../static/images/categories/simulation.svg';
import sportCategory from '../../../../static/images/categories/sport.svg';
import strategyCategory from '../../../../static/images/categories/strategy.svg';

export const ACTION_TYPES = {
  FETCH_CATEGORY_LIST: 'menu/FETCH_CATEGORY_LIST',
  MENU_STATE_CHANGED: 'menu/MENU_STATE_CHANGED'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMenu>,
  totalItems: 0,
  menuOpen: false
};

export type MenuState = Readonly<typeof initialState>;

// Reducer

const pickIcon = (id: number): string => {
  switch (id) {
    case 1:
      return allCategory;
    case 2:
      return actionCategory;
    case 3:
      return adventureCategory;
    case 4:
      return arcadeCategory;
    case 5:
      return boardCategory;
    case 6:
      return cardCategory;
    case 7:
      return casinoCategory;
    case 8:
      return casualCategory;
    case 9:
      return educationalCategory;
    case 10:
      return musicCategory;
    case 11:
      return puzzleCategory;
    case 12:
      return racingCategory;
    case 13:
      return rolePlayingCategory;
    case 14:
      return simulationCategory;
    case 15:
      return sportCategory;
    case 16:
      return strategyCategory;
    default:
      return allCategory;
  }
};

const addIcons = (categories: ICategory[]): IMenu[] =>
  categories.map(category => ({
    ...category,
    icon: pickIcon(category.id)
  }));

export default (state: MenuState = initialState, action): MenuState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CATEGORY_LIST):
      return {
        ...state,
        errorMessage: null,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CATEGORY_LIST):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CATEGORY_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: addIcons(action.payload.data)
      };
    case ACTION_TYPES.MENU_STATE_CHANGED:
      return {
        ...state,
        menuOpen: action.menuOpen
      };
    default:
      return state;
  }
};

const apiUrl = 'api/categories';
const apiRuUrl = 'api/category-rus';

// Actions

export const getEntities: ICrudGetAllAction<ICategory> = currentLocale => {
  const requestUrl = currentLocale === undefined || currentLocale.toString() === 'en' ? `${apiUrl}` : `${apiRuUrl}`;
  return {
    type: ACTION_TYPES.FETCH_CATEGORY_LIST,
    payload: axios.get<ICategory>(`${requestUrl}?cacheBuster=${new Date().getTime()}`)
  };
};

export const setMenuState = menuState => ({
  type: ACTION_TYPES.MENU_STATE_CHANGED,
  menuOpen: menuState
});
