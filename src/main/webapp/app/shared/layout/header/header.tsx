import './header.scss';

import React from 'react';

import { Translate } from 'react-jhipster';
import { Navbar, Nav, NavbarToggler, Collapse, Button } from 'reactstrap';
import LoadingBar from 'react-redux-loading-bar';

import { Home, Contact, About } from './header-components';
import { AdminMenu, EntitiesMenu, AccountMenu, LocaleMenu } from './menus';
import { Icon } from 'react-icons-kit';
// tslint:disable:no-submodule-imports
import { th } from 'react-icons-kit/fa/';
import has = Reflect.has;
// tslint:enable:no-submodule-imports

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isSwaggerEnabled: boolean;
  currentLocale: string;
  onLocaleChange: Function;
  onMenuChange: Function;
}

export interface IHeaderState {
  menuAdminOpen: boolean;
  menuOpen: boolean;
}

export default class Header extends React.Component<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    menuAdminOpen: false,
    menuOpen: false
  };

  handleLocaleChange = event => {
    this.props.onLocaleChange(event.target.value);
  };

  renderDevRibbon = () =>
    this.props.isInProduction === false ? (
      <div className="ribbon dev">
        <a href="">
          <Translate contentKey={`global.ribbon.${this.props.ribbonEnv}`} />
        </a>
      </div>
    ) : null;

  toggleAdminMenu = () => {
    this.setState({ menuAdminOpen: !this.state.menuAdminOpen });
  };

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen }, this.menuChange);
  };

  menuChange = () => {
    this.props.onMenuChange(this.state.menuOpen);
  };

  render() {
    const { currentLocale, isAuthenticated, isAdmin, isSwaggerEnabled, isInProduction } = this.props;

    const localeEnabled = !(window.location.hash.includes('game') || window.location.hash.includes('details'));

    /* jhipster-needle-add-element-to-menu - JHipster will add new menu items here */

    return (
      <div id="app-header">
        {this.renderDevRibbon()}
        <LoadingBar className="loading-bar" />
        <Navbar dark expand="sm" fixed="top" className="jh-navbar">
          <Button className={'app-menu-button' + (this.state.menuOpen ? ' app-menu-button-open' : '')} onClick={this.toggleMenu}>
            <Icon size={30} icon={th} />
          </Button>
          <NavbarToggler aria-label="Menu" onClick={this.toggleAdminMenu} className="jh-navbar-toggler" />
          <Collapse isOpen={this.state.menuAdminOpen} navbar>
            <Nav id="header-tabs" className="ml-auto" navbar>
              <Home />
              {isAuthenticated && <EntitiesMenu />}
              {isAuthenticated && isAdmin && <AdminMenu showSwagger={isSwaggerEnabled} showDatabase={!isInProduction} />}
              {localeEnabled && <LocaleMenu currentLocale={currentLocale} onClick={this.handleLocaleChange} />}
              {process.env.NODE_ENV === 'development' ? <AccountMenu isAuthenticated={isAuthenticated} /> : null}
              <Contact />
              <About />
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
