import React from 'react';
import { Translate } from 'react-jhipster';

import { UncontrolledDropdown, DropdownToggle, DropdownMenu, NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const NavDropdown = props => (
  <UncontrolledDropdown nav inNavbar id={props.id}>
    <DropdownToggle nav caret className="d-flex align-items-center justify-content-end">
      <FontAwesomeIcon icon={props.icon} />
      <span>{props.name}</span>
    </DropdownToggle>
    <DropdownMenu right style={props.style}>
      {props.children}
    </DropdownMenu>
  </UncontrolledDropdown>
);

export const BrandIcon = props => (
  <div {...props} className="brand-icon">
    <img src="content/images/sand-toys.svg" alt="Logo" />
  </div>
);

export const Brand = props => (
  <NavbarBrand tag={Link} to="" className="brand-logo">
    <BrandIcon />
    {/* <span className="brand-title">
      <Translate contentKey="global.title">OnlineGamesForKids</Translate>
    </span>
    <span className="navbar-version">{appConfig.VERSION}</span>*/}
  </NavbarBrand>
);

export const Home = props => (
  <NavItem>
    <NavLink tag={Link} to="/" className="d-flex align-items-center justify-content-end">
      <FontAwesomeIcon icon="home" />
      <span>
        <Translate contentKey="global.menu.home">Home</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Contact = () => (
  <NavItem>
    <NavLink tag={Link} to="/contact" className="d-flex align-items-center justify-content-end">
      <span>
        <Translate contentKey="global.menu.contact">Contact</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const About = () => (
  <NavItem>
    <NavLink tag={Link} to="/about" className="d-flex align-items-center justify-content-end">
      <span>
        <Translate contentKey="global.menu.about">About</Translate>
      </span>
    </NavLink>
  </NavItem>
);
