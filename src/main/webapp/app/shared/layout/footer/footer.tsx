import './footer.scss';

import React from 'react';
import { Translate } from 'react-jhipster';

const Footer = props => (
  <div className="footer page-content">
    <p className="text-right">
      <Translate contentKey="footer">Your footer</Translate>
    </p>
  </div>
);

export default Footer;
