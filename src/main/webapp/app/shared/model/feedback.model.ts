export interface IFeedback {
  id?: number;
  name?: string;
  message?: string;
  email?: string;
  subject?: string;
}

export const defaultValue: Readonly<IFeedback> = {
  id: 0,
  name: '',
  message: '',
  email: '',
  subject: ''
};
