import { IAssetType } from 'app/shared/model/asset-type.model';

export interface IAsset {
  id?: number;
  name?: string;
  description?: string;
  type?: IAssetType;
  location?: string;
}
