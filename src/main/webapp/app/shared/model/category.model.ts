import { IProduct } from 'app/shared/model//product.model';

export interface ICategory {
  id?: number;
  name?: string;
  description?: string;
  enabled?: boolean;
  products?: IProduct[];
}

export const defaultValue: Readonly<ICategory> = {
  enabled: true
};
