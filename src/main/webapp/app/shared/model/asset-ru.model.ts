import { IAssetType } from 'app/shared/model/asset-type.model';

export interface IAssetRu {
  id?: number;
  name?: string;
  description?: string;
  type?: IAssetType;
  location?: string;
}
