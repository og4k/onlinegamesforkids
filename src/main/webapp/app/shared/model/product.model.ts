import { ICategory } from 'app/shared/model//category.model';
import { IAsset } from 'app/shared/model/asset.model';
import { ILanguage } from 'app/shared/model/language.model';
import { IDetails } from 'app/shared/model/details.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string;
  url?: string;
  embedded?: string;
  assets?: IAsset[];
  enabled?: boolean;
  localizations?: ILanguage[];
  categories?: ICategory[];
  details?: IDetails;
}

export const defaultValue: Readonly<IProduct> = {
  enabled: true
};
