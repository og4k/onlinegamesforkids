export interface ILanguage {
  id?: number;
  name?: string;
  twoLetterName?: string;
  threeLetterName?: string;
  description?: string;
}
