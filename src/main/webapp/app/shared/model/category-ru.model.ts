import { IProductRu } from 'app/shared/model//product-ru.model';

export interface ICategoryRu {
  id?: number;
  name?: string;
  description?: string;
  enabled?: boolean;
  products?: IProductRu[];
}

export const defaultValue: Readonly<ICategoryRu> = {
  enabled: true
};
