import { IYoutube } from 'app/shared/model/youtube.model';

export interface IDetails {
  id?: number;
  name?: string;
  description?: string;
  youtubeClips?: IYoutube[];
}
