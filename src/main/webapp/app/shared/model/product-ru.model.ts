import { ICategoryRu } from 'app/shared/model//category-ru.model';
import { IAssetRu } from 'app/shared/model/asset-ru.model';
import { ILanguageRu } from 'app/shared/model/language-ru.model';
import { IDetails } from 'app/shared/model/details.model';

export interface IProductRu {
  id?: number;
  name?: string;
  description?: string;
  url?: string;
  embedded?: string;
  assets?: IAssetRu[];
  enabled?: boolean;
  localizations?: ILanguageRu[];
  categories?: ICategoryRu[];
  details?: IDetails;
}

export const defaultValue: Readonly<IProductRu> = {
  enabled: true
};
