import { ILanguage } from 'app/shared/model/language.model';

export interface IYoutube {
  id?: number;
  name?: string;
  description?: string;
  url?: string;
  enabled?: boolean;
  localizations?: ILanguage[];
}
