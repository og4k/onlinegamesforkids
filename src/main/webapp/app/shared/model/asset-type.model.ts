export interface IAssetType {
  id?: number;
  name?: string;
  description?: string;
}

export enum AssetType {
  Image = 1,
  Video = 2
}
