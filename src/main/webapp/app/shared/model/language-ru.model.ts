export interface ILanguageRu {
  id?: number;
  name?: string;
  twoLetterName?: string;
  threeLetterName?: string;
  description?: string;
}
