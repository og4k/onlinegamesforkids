import { ICategory } from 'app/shared/model/category.model';

export interface IMenu extends ICategory {
  icon?: string;
}

export const defaultValue: Readonly<IMenu> = {};
