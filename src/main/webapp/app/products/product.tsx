import './products.scss';

import React from 'react';
import { RouteComponentProps } from 'react-router';
import { getPaginationItemsNumber, getSortState, getUrlParameter, IPaginationBaseState, Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { getEntities, searchEntities } from 'app/products/product.reducer';
import { connect } from 'react-redux';
import { TWENTY_ITEMS_PER_PAGE, FIFTY_ITEMS_PER_PAGE, HUNDRED_ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import {
  Card,
  CardBody,
  CardText,
  Row,
  UncontrolledTooltip,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  ButtonGroup,
  Button,
  InputGroup,
  InputGroupAddon,
  Input
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DotsLoading from 'app/shared/loading/dots-loading';
import CardVideo from 'app/shared/card-video/card-video';
import { JhiPagination2 } from 'app/shared/pagination/JhiPagination2';
// tslint:disable:no-submodule-imports
import { chevronCircleRight } from 'react-icons-kit/fa/';
// tslint:enable:no-submodule-imports

export interface IProductProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string; searchValue: string }> {}
export interface IProductState extends IPaginationBaseState {
  currentLocale: undefined;
  id: number;
  goValue: string;
  goEnabled: boolean;
  searchValueUrl: string;
  searchValue: string;
  gameButtonOverId: number;
  youtubeButtonOverId: number;
}

export class Product extends React.Component<IProductProps, IProductState> {
  state: IProductState = {
    ...getSortState(this.props.location, TWENTY_ITEMS_PER_PAGE),
    currentLocale: this.props.currentLocale,
    id: this.props.match.params.id ? +this.props.match.params.id : 1,
    goValue: '',
    goEnabled: false,
    searchValueUrl: this.props.match.params.searchValue ? this.props.match.params.searchValue : '',
    searchValue: this.props.match.params.searchValue ? this.props.match.params.searchValue : '',
    gameButtonOverId: 0,
    youtubeButtonOverId: 0
  };

  cardVideoComponents: { [guid: string]: CardVideo } = {};

  componentDidMount() {
    if (this.state.currentLocale !== undefined) {
      if (this.state.searchValueUrl) {
        this.searchEntities();
      } else {
        this.getEntities();
      }
    }
  }

  componentDidUpdate() {
    const sizeParameter = getUrlParameter('size', this.props.location.search);
    const size = sizeParameter ? +sizeParameter : TWENTY_ITEMS_PER_PAGE;

    if (!Number.isInteger(size) || (size !== TWENTY_ITEMS_PER_PAGE && size !== FIFTY_ITEMS_PER_PAGE && size !== HUNDRED_ITEMS_PER_PAGE)) {
      return;
    }

    const { activePage, itemsPerPage, sort, order } = getSortState(this.props.location, size);
    const currentLocale = this.props.currentLocale;
    const id = this.props.match.params.id ? +this.props.match.params.id : 1;
    const searchValueUrl = this.props.match.params.searchValue ? this.props.match.params.searchValue : '';

    const update = f => {
      if (currentLocale !== this.state.currentLocale && (activePage !== 1 || itemsPerPage !== TWENTY_ITEMS_PER_PAGE)) {
        const { pathname } = this.props.location;
        const url = this.generatePageUrl(1, TWENTY_ITEMS_PER_PAGE, `${this.state.sort},${this.state.order}`);
        this.props.history.push(pathname + url);
      } else {
        f();
      }
    };

    if (
      activePage !== this.state.activePage ||
      itemsPerPage !== this.state.itemsPerPage ||
      sort !== this.state.sort ||
      order !== this.state.order ||
      id !== this.state.id ||
      currentLocale !== this.state.currentLocale ||
      searchValueUrl !== this.state.searchValueUrl
    ) {
      if (searchValueUrl) {
        update(() =>
          this.setState({ activePage, itemsPerPage, sort, order, id, currentLocale, searchValueUrl }, () => this.searchEntities())
        );
      } else {
        update(() => this.setState({ activePage, itemsPerPage, sort, order, id, currentLocale, searchValueUrl }, () => this.getEntities()));
      }
    }
  }

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order, currentLocale, id } = this.state;
    this.props.getEntities(id, activePage, itemsPerPage, `${sort},${order}`, currentLocale);
  };

  searchEntities = () => {
    const { activePage, itemsPerPage, sort, order, currentLocale, searchValueUrl } = this.state;
    this.props.searchEntities(searchValueUrl, activePage, itemsPerPage, `${sort},${order}`, currentLocale);
  };

  getStyle = (): string => {
    const { id } = this.state;

    const remainder = id % 3;
    if (remainder === 1) {
      return 'card-type-1';
    } else if (remainder === 2) {
      return 'card-type-2';
    } else if (!remainder) {
      return 'card-type-3';
    }

    return '';
  };

  itemClicked = event => {
    const { itemsPerPage, sort, order } = this.state;
    const value = +event.target.innerText;

    if (itemsPerPage !== value) {
      const { pathname } = this.props.location;
      const url = this.generatePageUrl(1, value, `${sort},${order}`);
      this.props.history.push(pathname + url);
    }
  };

  onAZClicked = () => {
    this.onSortBtnClicked('name', 'asc');
  };

  onZAClicked = () => {
    this.onSortBtnClicked('name', 'desc');
  };

  onNewestClicked = () => {
    this.onSortBtnClicked('createdDate', 'desc');
  };

  onSortBtnClicked = (sort2: string, order2: string) => {
    const { sort, order } = this.state;

    if (sort !== sort2 || order !== order2) {
      const { activePage, itemsPerPage } = this.state;
      const { pathname } = this.props.location;
      const url = this.generatePageUrl(activePage, itemsPerPage, `${sort2},${order2}`);
      this.props.history.push(pathname + url);
    }
  };

  onPlayStarted = (guid: string) => {
    Object.keys(this.cardVideoComponents).forEach(key => {
      if (key !== guid) {
        this.cardVideoComponents[key].stop();
      }
    });
  };

  onComponentDidMount = (guid: string, component: CardVideo) => {
    this.cardVideoComponents[guid] = component;
  };

  onComponentWillUnmount = (guid: string) => {
    delete this.cardVideoComponents[guid];
  };

  handlePagination = activePage => {
    const check = () => {
      const currentScrollOffset = window.pageYOffset || document.documentElement.scrollTop;
      if (!currentScrollOffset) {
        setTimeout(() => {
          const { itemsPerPage, sort, order } = this.state;
          const { pathname } = this.props.location;
          const url = this.generatePageUrl(activePage, itemsPerPage, `${sort},${order}`);
          this.props.history.push(pathname + url);
        }, 200);

        window.onscroll = null;
      }
    };

    check();

    window.onscroll = () => {
      check();
    };
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  handleGoChange = e => {
    const goValue = +e.target.value;
    if (Number.isInteger(goValue) && goValue > 0 && goValue <= getPaginationItemsNumber(this.props.totalItems, this.state.itemsPerPage)) {
      this.setState({ goValue: e.target.value, goEnabled: true });
    } else {
      this.setState({ goValue: e.target.value, goEnabled: false });
    }
  };

  handleGoKeyPress = event => {
    const { goEnabled, goValue } = this.state;
    if (event.charCode === 13 && goEnabled) {
      const value = +goValue;
      this.setState({ goValue: '', goEnabled: false }, () => this.handlePagination(value));
    }
  };

  handleGoClick = () => {
    const value = +this.state.goValue;
    this.setState({ goValue: '', goEnabled: false }, () => this.handlePagination(value));
  };

  handleClearClick = () => {
    this.setState({ searchValue: '' });
  };

  handleSearchChange = e => {
    this.setState({ searchValue: e.target.value });
  };

  handleSearchClick = () => {
    const { searchValue } = this.state;
    this.props.history.push(`/search/${searchValue}`);
  };

  handleSearchKeyPress = event => {
    if (event.charCode === 13) {
      const { searchValue } = this.state;
      this.props.history.push(`/search/${searchValue}`);
    }
  };

  playClickHandler = product => () => {
    if (product.url.includes('|')) {
      window.open(`#/game/${product.id}`, '_blank');
    } else {
      window.open(product.url, '_blank');
    }
  };

  watchClickHandler = product => () => {
    window.open(`#/details/${product.details.id}`, '_blank');
  };

  gameMouseEnterHandler = product => () => {
    this.setState({ gameButtonOverId: product.id });
  };

  gameMouseLeftHandler = () => {
    this.setState({ gameButtonOverId: 0 });
  };

  youtubeMouseEnterHandler = product => () => {
    this.setState({ youtubeButtonOverId: product.id });
  };

  youtubeMouseLeftHandler = () => {
    this.setState({ youtubeButtonOverId: 0 });
  };

  generatePageUrl = (page, size, sort) => `?page=${page}&size=${size}&sort=${sort}`;

  render() {
    const { productList, totalItems, loading } = this.props;
    const { itemsPerPage, activePage, sort, order, goEnabled, searchValue, gameButtonOverId, youtubeButtonOverId } = this.state;

    return (
      <div className="products">
        {loading ? (
          <DotsLoading showText delay={3000} />
        ) : (
          <div>
            <div className="top-content">
              <div className="sorting-dashboard">
                <ButtonGroup>
                  <Button outline color="primary" onClick={this.onAZClicked} active={sort === 'name' && order === 'asc'}>
                    <Translate contentKey="products.az">A-Z</Translate>
                  </Button>
                  <Button outline color="primary" onClick={this.onZAClicked} active={sort === 'name' && order === 'desc'}>
                    <Translate contentKey="products.za">Z-A</Translate>
                  </Button>
                  <Button outline color="primary" onClick={this.onNewestClicked} active={sort === 'createdDate' && order === 'desc'}>
                    <Translate contentKey="products.newest">Newest</Translate>
                  </Button>
                </ButtonGroup>
              </div>
              <div className="search">
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <Button color="danger" disabled={!searchValue.length} onClick={this.handleClearClick}>
                      <FontAwesomeIcon icon="times-circle" />
                    </Button>
                  </InputGroupAddon>
                  <Input type="text" value={searchValue} onChange={this.handleSearchChange} onKeyPress={this.handleSearchKeyPress} />
                  <InputGroupAddon addonType="append">
                    <Button color="primary" disabled={!searchValue.length} onClick={this.handleSearchClick}>
                      <FontAwesomeIcon icon="search" />
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
              </div>
            </div>

            <div className="content">
              {productList.map((product, i) => (
                <Card key={`entity-${i}`} className={this.getStyle()}>
                  <div className="video-container">
                    <CardVideo
                      product={product}
                      playStarted={this.onPlayStarted}
                      componentDidMount={this.onComponentDidMount}
                      componentWillUnmount={this.onComponentWillUnmount}
                    />
                  </div>
                  <CardBody>
                    {/*<CardTitle>
                      <a href={product.url} target="_blank" title={product.url}>
                        <Icon size={24} icon={chevronCircleRight} />
                        <span className="product-name">{product.name}</span>
                        {product.localizations.map((localization, index) => (
                          <span key={`localization-${i}-${index}`}>
                            <Badge color="light" id={`tooltip-${i}-${index}`}>
                              {localization.twoLetterName}
                            </Badge>
                            <UncontrolledTooltip placement="top" target={`tooltip-${i}-${index}`}>
                              {localization.description}
                            </UncontrolledTooltip>
                          </span>
                        ))}
                      </a>
                    </CardTitle>*/}
                    <CardText id={`tooltip-${i}-description`} className="text-center">
                      {product.name}
                      <UncontrolledTooltip
                        placement="top"
                        target={`tooltip-${i}-description`}
                        delay={{ show: 500, hide: 250 }}
                        className="description-tooltip"
                      >
                        {product.description}
                      </UncontrolledTooltip>
                    </CardText>
                    <div className="card-buttons">
                      <Button
                        color="primary"
                        onClick={this.playClickHandler(product)}
                        onMouseEnter={this.gameMouseEnterHandler(product)}
                        onMouseLeave={this.gameMouseLeftHandler}
                      >
                        <FontAwesomeIcon icon="gamepad" size="lg" spin={product.id === gameButtonOverId} />
                      </Button>
                      <Button
                        color="danger"
                        onClick={this.watchClickHandler(product)}
                        disabled={!product.details}
                        onMouseEnter={this.youtubeMouseEnterHandler(product)}
                        onMouseLeave={this.youtubeMouseLeftHandler}
                      >
                        <FontAwesomeIcon icon={['fab', 'youtube']} size="lg" spin={product.details && product.id === youtubeButtonOverId} />
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              ))}
            </div>
          </div>
        )}
        {!loading && (
          <Row className="justify-content-center">
            <JhiPagination2
              items={getPaginationItemsNumber(totalItems, itemsPerPage)}
              activePage={activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
            />
            <div className="move-to-page">
              <InputGroup>
                <Input type="text" onChange={this.handleGoChange} onKeyPress={this.handleGoKeyPress} />
                <InputGroupAddon addonType="append">
                  <Button color="primary" disabled={!goEnabled} onClick={this.handleGoClick}>
                    <Translate contentKey="products.go">Go</Translate>
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </div>
            <div className="items-per-page">
              <span className="label">
                <Translate contentKey="products.itemsPerPage">Items per page</Translate>
              </span>
              <UncontrolledDropdown>
                <DropdownToggle outline color="primary" caret>
                  {itemsPerPage}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem color="primary" onClick={this.itemClicked}>
                    {TWENTY_ITEMS_PER_PAGE}
                  </DropdownItem>
                  <DropdownItem color="primary" onClick={this.itemClicked}>
                    {FIFTY_ITEMS_PER_PAGE}
                  </DropdownItem>
                  <DropdownItem color="primary" onClick={this.itemClicked}>
                    {HUNDRED_ITEMS_PER_PAGE}
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </div>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ products, locale }: IRootState) => ({
  productList: products.entities,
  totalItems: products.totalItems,
  loading: products.loading,
  currentLocale: locale.currentLocale
});

const mapDispatchToProps = {
  getEntities,
  searchEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
