import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';
import axios from 'axios';
import { IProduct } from 'app/shared/model/product.model';

export const ACTION_TYPES = {
  FETCH_PRODUCT_LIST: 'products/FETCH_PRODUCT_LIST',
  SEARCH_PRODUCT_LIST: 'products/SEARCH_PRODUCT_LIST'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProduct>,
  totalItems: 0
};

export type ProductState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductState = initialState, action): ProductState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCT_LIST):
    case REQUEST(ACTION_TYPES.SEARCH_PRODUCT_LIST):
      return {
        ...state,
        errorMessage: null,
        loading: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCT_LIST):
    case FAILURE(ACTION_TYPES.SEARCH_PRODUCT_LIST):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCT_LIST):
    case SUCCESS(ACTION_TYPES.SEARCH_PRODUCT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    default:
      return state;
  }
};

const getEntitiesApiUrl = 'api/products/category/';
const getEntitiesApiRuUrl = 'api/product-rus/category-rus/';

const searchEntitiesApiUrl = 'api/products/search/';
const searchEntitiesApiRuUrl = 'api/product-rus/search/';

// Actions

export const getEntities = (id, page, size, sort, currentLocale) => {
  const url = currentLocale === undefined || currentLocale.toString() === 'en' ? `${getEntitiesApiUrl}` : `${getEntitiesApiRuUrl}`;
  const requestUrl = `${url}${id}${sort ? `?page=${page - 1}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCT_LIST,
    payload: axios.get<IProduct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const searchEntities = (searchValue, page, size, sort, currentLocale) => {
  const url = currentLocale === undefined || currentLocale.toString() === 'en' ? `${searchEntitiesApiUrl}` : `${searchEntitiesApiRuUrl}`;
  const requestUrl = `${url}?name=${searchValue}${sort ? `&page=${page - 1}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.SEARCH_PRODUCT_LIST,
    payload: axios.get<IProduct>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};
