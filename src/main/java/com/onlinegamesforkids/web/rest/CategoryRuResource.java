package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.CategoryRu;
import com.onlinegamesforkids.repository.CategoryRuRepository;
import com.onlinegamesforkids.web.rest.errors.BadRequestAlertException;
import com.onlinegamesforkids.web.rest.util.HeaderUtil;
import com.onlinegamesforkids.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CategoryRu.
 */
@RestController
@RequestMapping("/api")
public class CategoryRuResource {

    private final Logger log = LoggerFactory.getLogger(CategoryRuResource.class);

    private static final String ENTITY_NAME = "categoryRu";

    private final CategoryRuRepository categoryRuRepository;

    public CategoryRuResource(CategoryRuRepository categoryRuRepository) {
        this.categoryRuRepository = categoryRuRepository;
    }

    /**
     * POST  /category-rus : Create a new categoryRu.
     *
     * @param categoryRu the categoryRu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new categoryRu, or with status 400 (Bad Request) if the categoryRu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/category-rus")
    @Timed
    public ResponseEntity<CategoryRu> createCategoryRu(@Valid @RequestBody CategoryRu categoryRu) throws URISyntaxException {
        log.debug("REST request to save CategoryRu : {}", categoryRu);
        if (categoryRu.getId() != null) {
            throw new BadRequestAlertException("A new categoryRu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategoryRu result = categoryRuRepository.save(categoryRu);
        return ResponseEntity.created(new URI("/api/category-rus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /category-rus : Updates an existing categoryRu.
     *
     * @param categoryRu the categoryRu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated categoryRu,
     * or with status 400 (Bad Request) if the categoryRu is not valid,
     * or with status 500 (Internal Server Error) if the categoryRu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/category-rus")
    @Timed
    public ResponseEntity<CategoryRu> updateCategoryRu(@Valid @RequestBody CategoryRu categoryRu) throws URISyntaxException {
        log.debug("REST request to update CategoryRu : {}", categoryRu);
        if (categoryRu.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CategoryRu result = categoryRuRepository.save(categoryRu);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, categoryRu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /category-rus : get all the categoryRus.
     *
     * @param pageable the pagination information
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of categoryRus in body
     */
    @GetMapping("/category-rus")
    @Timed
    public ResponseEntity<List<CategoryRu>> getAllCategoryRus(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of CategoryRus");
        Page<CategoryRu> page;
        if (eagerload) {
            page = categoryRuRepository.findAllWithEagerRelationships(pageable);
        } else {
            page = categoryRuRepository.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/category-rus?eagerload=%b", eagerload));
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /category-rus/:id : get the "id" categoryRu.
     *
     * @param id the id of the categoryRu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the categoryRu, or with status 404 (Not Found)
     */
    @GetMapping("/category-rus/{id}")
    @Timed
    public ResponseEntity<CategoryRu> getCategoryRu(@PathVariable Long id) {
        log.debug("REST request to get CategoryRu : {}", id);
        Optional<CategoryRu> categoryRu = categoryRuRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(categoryRu);
    }

    /**
     * DELETE  /category-rus/:id : delete the "id" categoryRu.
     *
     * @param id the id of the categoryRu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/category-rus/{id}")
    @Timed
    public ResponseEntity<Void> deleteCategoryRu(@PathVariable Long id) {
        log.debug("REST request to delete CategoryRu : {}", id);

        categoryRuRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
