package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.DetailsRu;
import com.onlinegamesforkids.repository.DetailsRuRepository;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * REST controller for managing DetailsRu.
 */
@RestController
@RequestMapping("/api")
public class DetailsRuResource {

    private final Logger log = LoggerFactory.getLogger(DetailsRuResource.class);

    private static final String ENTITY_NAME = "detailsRu";

    private final DetailsRuRepository detailsRepository;

    public DetailsRuResource(DetailsRuRepository detailsRepository) {
        this.detailsRepository = detailsRepository;
    }

    /**
     * GET  /details/:id : get {@link DetailsRu} by "id".
     *
     * @param id the id of the detailsRu.
     * @return the ResponseEntity with status 200 (OK) and with body {@link DetailsRu}, or with status 404 (Not Found).
     */
    @GetMapping("/details-rus/{id}")
    @Timed
    public ResponseEntity<DetailsRu> getDetailsRu(@PathVariable Long id) {
        log.debug("REST request to get DetailsRu : {}", id);
        Optional<DetailsRu> details = detailsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(details);
    }
}
