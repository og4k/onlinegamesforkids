package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.Composition;
import com.onlinegamesforkids.domain.CompositionRu;
import com.onlinegamesforkids.domain.Product;
import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.repository.CompositionRuRepository;
import com.onlinegamesforkids.repository.ProductRepository;
import com.onlinegamesforkids.repository.ProductRuRepository;
import com.onlinegamesforkids.web.rest.errors.BadRequestAlertException;
import com.onlinegamesforkids.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * REST controller for managing CompositionRu.
 */
@RestController
@RequestMapping("/api")
public class CompositionRuResource {

    private final Logger log = LoggerFactory.getLogger(CompositionRuResource.class);

    private static final String ENTITY_NAME = "compositionRu";

    private final CompositionRuRepository compositionRuRepository;

    private final ProductRuRepository productRuRepository;

    public CompositionRuResource(CompositionRuRepository compositionRuRepository,
                                 ProductRuRepository productRuRepository) {
        this.compositionRuRepository = compositionRuRepository;
        this.productRuRepository = productRuRepository;
    }

    /**
     * GET  /composition-rus/:id : get {@link List<ProductRu>} by "id" compositionRu.
     *
     * @param id the id of the compositionRu.
     * @return the ResponseEntity with status 200 (OK) and with body {@link List<ProductRu>}, or with status 404 (Not Found).
     */
    @GetMapping("/composition-rus/{id}")
    @Timed
    public ResponseEntity<List<ProductRu>> getCompositionRu(@PathVariable Long id) {
        log.debug("REST request to get CompositionRu : {}", id);
        Optional<CompositionRu> compositionRu = compositionRuRepository.findById(id);

        if (compositionRu.isPresent()) {
            CompositionRu foundCompositionRu = compositionRu.get();
            Set<Long> productRuIds = foundCompositionRu
                .getProducts()
                .stream()
                .map(ProductRu::getId)
                .collect(Collectors.toSet());

            log.debug("REST request to get ProductRus : {}", String.join(",", Arrays.toString(productRuIds.toArray(new Long[0]))));

            List<ProductRu> foundProductsRu = productRuRepository.findProductsRuByIds(productRuIds);

            return new ResponseEntity<>(foundProductsRu, HeaderUtil.createTotalCount(foundProductsRu.size()), HttpStatus.OK);
        }

        throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
}
