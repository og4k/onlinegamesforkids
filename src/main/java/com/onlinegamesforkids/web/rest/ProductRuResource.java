package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.repository.ProductRuRepository;
import com.onlinegamesforkids.web.rest.errors.BadRequestAlertException;
import com.onlinegamesforkids.web.rest.util.HeaderUtil;
import com.onlinegamesforkids.web.rest.util.PaginationUtil;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProductRu.
 */
@RestController
@RequestMapping("/api")
public class ProductRuResource {

    private final Logger log = LoggerFactory.getLogger(ProductRuResource.class);

    private static final String ENTITY_NAME = "productRu";

    private final ProductRuRepository productRuRepository;

    public ProductRuResource(ProductRuRepository productRuRepository) {
        this.productRuRepository = productRuRepository;
    }

    /**
     * POST  /product-rus : Create a new productRu.
     *
     * @param productRu the productRu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productRu, or with status 400 (Bad Request) if the productRu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-rus")
    @Timed
    public ResponseEntity<ProductRu> createProductRu(@Valid @RequestBody ProductRu productRu) throws URISyntaxException {
        log.debug("REST request to save ProductRu : {}", productRu);
        if (productRu.getId() != null) {
            throw new BadRequestAlertException("A new productRu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductRu result = productRuRepository.save(productRu);
        return ResponseEntity.created(new URI("/api/product-rus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /product-rus : Updates an existing productRu.
     *
     * @param productRu the productRu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productRu,
     * or with status 400 (Bad Request) if the productRu is not valid,
     * or with status 500 (Internal Server Error) if the productRu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-rus")
    @Timed
    public ResponseEntity<ProductRu> updateProductRu(@Valid @RequestBody ProductRu productRu) throws URISyntaxException {
        log.debug("REST request to update ProductRu : {}", productRu);
        if (productRu.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductRu result = productRuRepository.save(productRu);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productRu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /product-rus : get all the productRus.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productRus in body
     */
    @GetMapping("/product-rus")
    @Timed
    public ResponseEntity<List<ProductRu>> getAllProductRus(Pageable pageable) {
        log.debug("REST request to get a page of ProductRus");
        Page<ProductRu> page = productRuRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-rus");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-rus/:id : get the "id" productRu.
     *
     * @param id the id of the productRu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productRu, or with status 404 (Not Found)
     */
    @GetMapping("/product-rus/{id}")
    @Timed
    public ResponseEntity<ProductRu> getProductRu(@PathVariable Long id) {
        log.debug("REST request to get ProductRu : {}", id);
        Optional<ProductRu> productRu = productRuRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productRu);
    }

    /**
     * GET  /product-rus/eager/:id : get the "id" productRu including fetching localizations and assets.
     *
     * @param id the id of the productRu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productRu, or with status 404 (Not Found)
     */
    @GetMapping("/product-rus/eager/{id}")
    @Timed
    public ResponseEntity<ProductRu> getProductRuIncludeAssetsAndLocalizations(@PathVariable Long id) {
        log.debug("REST request to get ProductRu including fetching localizations and assets: {}", id);
        Optional<ProductRu> productRu = productRuRepository.findProductRuByIdIncludeAssetsAndLocalizations(id);
        return ResponseUtil.wrapOrNotFound(productRu);
    }

    /**
     * DELETE  /product-rus/:id : delete the "id" productRu.
     *
     * @param id the id of the productRu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-rus/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductRu(@PathVariable Long id) {
        log.debug("REST request to delete ProductRu : {}", id);

        productRuRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /product-rus/category-rus/:id : get all products for the "id" category-rus.
     *
     * @param id the id of the category-rus to retrieve
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of product-rus in body
     */
    @GetMapping("/product-rus/category-rus/{id}")
    @Timed
    public ResponseEntity<List<ProductRu>> getProductRuPerCategoryRu(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get a page of ProductRu per CategoryRu");
        Page<ProductRu> page = productRuRepository.findAllProductRuForOneCategoryRu(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/product-rus/category-rus/%d?eagerload=%b", id, true));
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-rus/search/:searchValue : search across all products rus.
     *
     * @param predicate uses {@link QuerydslPredicate}.
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of products rus in body
     */
    @GetMapping("/product-rus/search")
    @Timed
    public ResponseEntity<List<ProductRu>> searchProducts(@QuerydslPredicate(root = ProductRu.class)Predicate predicate, Pageable pageable) {
        log.debug("REST request to search for products rus");

        Page<ProductRu> page = productRuRepository.search(predicate, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/product-rus/search?eagerload=%b", true));
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
