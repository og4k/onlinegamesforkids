/**
 * View Models used by Spring MVC REST controllers.
 */
package com.onlinegamesforkids.web.rest.vm;
