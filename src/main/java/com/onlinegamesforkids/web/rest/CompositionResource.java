package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.Composition;
import com.onlinegamesforkids.domain.Product;
import com.onlinegamesforkids.repository.CompositionRepository;
import com.onlinegamesforkids.repository.ProductRepository;
import com.onlinegamesforkids.web.rest.errors.BadRequestAlertException;
import com.onlinegamesforkids.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing Composition.
 */
@RestController
@RequestMapping("/api")
public class CompositionResource {

    private final Logger log = LoggerFactory.getLogger(CompositionResource.class);

    private static final String ENTITY_NAME = "composition";

    private final CompositionRepository compositionRepository;

    private final ProductRepository productRepository;

    public CompositionResource(CompositionRepository compositionRepository,
                               ProductRepository productRepository) {
        this.compositionRepository = compositionRepository;
        this.productRepository = productRepository;
    }

    /**
     * GET  /compositions/:id : get {@link List<Product>} by "id" composition.
     *
     * @param id the id of the composition.
     * @return the ResponseEntity with status 200 (OK) and with body {@link List<Product>}, or with status 404 (Not Found).
     */
    @GetMapping("/compositions/{id}")
    @Timed
    public ResponseEntity<List<Product>> getComposition(@PathVariable Long id) {
        log.debug("REST request to get Composition : {}", id);
        Optional<Composition> composition = compositionRepository.findById(id);

        if (composition.isPresent()) {
            Composition foundComposition = composition.get();
            Set<Long> productIds = foundComposition
                .getProducts()
                .stream()
                .map(Product::getId)
                .collect(Collectors.toSet());

            log.debug("REST request to get Products : {}", String.join(",", Arrays.toString(productIds.toArray(new Long[0]))));

            List<Product> foundProducts = productRepository.findProductsByIds(productIds);

            return new ResponseEntity<>(foundProducts, HeaderUtil.createTotalCount(foundProducts.size()), HttpStatus.OK);
        }

        throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
}
