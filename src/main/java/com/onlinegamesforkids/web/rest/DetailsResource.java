package com.onlinegamesforkids.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.onlinegamesforkids.domain.Details;
import com.onlinegamesforkids.repository.DetailsRepository;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * REST controller for managing Details.
 */
@RestController
@RequestMapping("/api")
public class DetailsResource {

    private final Logger log = LoggerFactory.getLogger(DetailsResource.class);

    private static final String ENTITY_NAME = "details";

    private final DetailsRepository detailsRepository;

    public DetailsResource(DetailsRepository detailsRepository) {
        this.detailsRepository = detailsRepository;
    }

    /**
     * GET  /details/:id : get {@link Details} by "id".
     *
     * @param id the id of the details.
     * @return the ResponseEntity with status 200 (OK) and with body {@link Details}, or with status 404 (Not Found).
     */
    @GetMapping("/details/{id}")
    @Timed
    public ResponseEntity<Details> getDetails(@PathVariable Long id) {
        log.debug("REST request to get Details : {}", id);
        Optional<Details> details = detailsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(details);
    }
}
