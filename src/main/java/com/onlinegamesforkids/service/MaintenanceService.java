package com.onlinegamesforkids.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinegamesforkids.domain.Product;
import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.domain.Youtube;
import com.onlinegamesforkids.domain.YoutubeRu;
import com.onlinegamesforkids.repository.ProductRepository;
import com.onlinegamesforkids.repository.ProductRuRepository;
import com.onlinegamesforkids.repository.YoutubeRepository;
import com.onlinegamesforkids.repository.YoutubeRuRepository;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service for maintenance.
 * <p>
 * We use this service to make sure all our assets are available and if not we notify technical service.
 */
@Service
public class MaintenanceService {

    @Value("${application.mail.to:katherine.anokhina@gmail.com}")
    private String to;

    @Value("${application.mail.subject:THERE ARE NOT AVAILABLE RESOURCES}")
    private String subject;

    @Value("${application.youtubeApi.url:https://www.googleapis.com/youtube/v3/videos}")
    private String url;

    @Value("${application.youtubeApi.part:status}")
    private String part;

    @Value("${application.youtubeApi.key:AIzaSyCg6KVJNPGE-vYbz150kZBCHuqet4IC4mQ}")
    private String key;

    /**
     * {@link Logger}.
     */
    private final Logger log = LoggerFactory.getLogger(MaintenanceService.class);

    /**
     * {@link ProductRepository}.
     */
    private final ProductRepository productRepository;

    /**
     * {@link ProductRuRepository}.
     */
    private final ProductRuRepository productRuRepository;

    /**
     * {@link YoutubeRepository}.
     */
    private final YoutubeRepository youtubeRepository;

    /**
     * {@link YoutubeRuRepository}.
     */
    private final YoutubeRuRepository youtubeRuRepository;

    /**
     * {@link MailService}.
     */
    private final MailService mailService;

    /**
     * Constructor.
     * @param productRepository {@link ProductRepository}.
     * @param productRuRepository {@link ProductRuRepository}.
     * @param youtubeRepository {@link YoutubeRepository}.
     * @param youtubeRuRepository {@link YoutubeRuRepository}.
     * @param mailService {@link MailService}.
     */
    public MaintenanceService(ProductRepository productRepository,
                              ProductRuRepository productRuRepository,
                              YoutubeRepository youtubeRepository,
                              YoutubeRuRepository youtubeRuRepository,
                              MailService mailService) {
        this.productRepository = productRepository;
        this.productRuRepository = productRuRepository;
        this.youtubeRepository = youtubeRepository;
        this.youtubeRuRepository = youtubeRuRepository;
        this.mailService = mailService;
    }

    /**
     * We check all products' urls and if any is broken we notify technical service.
     */
    @Scheduled(cron="0 0 2 * * ?", zone="Australia/Sydney")
    public void check() {
        String productsMessage = checkForProducts();
        String youtubesMessage = checkForYoutubes();

        if (productsMessage.length() > 0 || youtubesMessage.length() > 0) {
            StringBuilder email = new StringBuilder(productsMessage);
            email.append(System.getProperty("line.separator"));
            email.append(youtubesMessage);

            log.debug("request to send email: {}", email.toString());

            mailService.sendEmail(this.to, this.subject, email.toString(), false, false);
        }
    }

    /**
     * We check all products and if any issues generate email.
     * @return generated report.
     */
    private String checkForProducts() {
        log.debug("REST request to check all products (English and Russian)");

        // get all eng products
        List<Product> products = productRepository.findAll();
        Set<String> productUrls = products
            .stream()
            .map(p -> !p.getUrl().contains("|") ? p.getUrl() : p.getUrl().split("\\|", -1)[0])
            .collect(Collectors.toSet());

        log.debug("got all product urls: {}", String.join(", ", productUrls));

        // get all ru products
        List<ProductRu> productsRu = productRuRepository.findAll();
        Set<String> productRuUrls = productsRu
            .stream()
            .map(p -> !p.getUrl().contains("|") ? p.getUrl() : p.getUrl().split("\\|", -1)[0])
            .collect(Collectors.toSet());

        log.debug("got all product Ru urls: {}", String.join(", ", productRuUrls));

        // check url is available
        int timeout = 10;
        RequestConfig config = RequestConfig.custom()
            .setConnectTimeout(timeout * 1000)
            .setConnectionRequestTimeout(timeout * 1000)
            .setSocketTimeout(timeout * 1000).build();
        HttpClient client = HttpClientBuilder
            .create()
            .setDefaultRequestConfig(config)
            .build();

        // merge urls
        Set<String> allUrls = new HashSet<>(productUrls);
        allUrls.addAll(productRuUrls);

        StringBuilder email = new StringBuilder();
        for (String url: allUrls) {

            HttpGet method = new HttpGet(url);

            HttpResponse response;
            int statusCode = 0;
            try {
                log.debug("request to check url: {}", url);

                response = client.execute(method);
                statusCode = response.getStatusLine().getStatusCode();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                method.releaseConnection();
            }

            if (statusCode != HttpStatus.SC_OK) {
                log.debug("got failed url: {} and status code: {}", url, statusCode);

                if (email.length() <= 0) {
                    email.append("PRODUCTS");
                    email.append(System.getProperty("line.separator"));
                }

                List<Product> failedProducts = products.stream().filter(p -> p.getUrl().contains(url)).collect(Collectors.toList());

                for (Product p: failedProducts) {
                    String message = String.format("Product id = %d, name = %s, url = %s and statusCode = %s", p.getId(), p.getName(), p.getUrl(), statusCode);

                    email.append(message);
                    email.append(System.getProperty("line.separator"));
                }

                List<ProductRu> failedProductsRu = productsRu.stream().filter(p -> p.getUrl().contains(url)).collect(Collectors.toList());

                for (ProductRu p: failedProductsRu) {
                    String message = String.format("ProductRu id = %d, name = %s, url = %s and statusCode = %s", p.getId(), p.getName(), p.getUrl(), statusCode);

                    email.append(message);
                    email.append(System.getProperty("line.separator"));
                }
            }
        }

        return email.toString();
    }

    /**
     * We check all youtubes and if any issues generate email.
     * @return generated report.
     */
    private String checkForYoutubes() {
        // youtube check
        log.debug("request to check youtube");

        // get all eng youtubes
        List<Youtube> youtubes = youtubeRepository.findAll();
        Set<String> youtubeKeys = youtubes
            .stream()
            .map(y -> y.getUrl().substring(y.getUrl().lastIndexOf("/") + 1))
            .collect(Collectors.toSet());

        log.debug("got all youtube keys: {}", String.join(", ", youtubeKeys));

        // get all ru youtubes
        List<YoutubeRu> youtubesRu = youtubeRuRepository.findAll();
        Set<String> youtubeRuKeys = youtubesRu
            .stream()
            .map(y -> y.getUrl().substring(y.getUrl().lastIndexOf("/") + 1))
            .collect(Collectors.toSet());

        log.debug("got all youtube ru keys: {}", String.join(", ", youtubeRuKeys));

        // check url is available
        HttpClient client = HttpClientBuilder.create().build();

        // merge urls
        Set<String> allKeys = new HashSet<>(youtubeKeys);
        allKeys.addAll(youtubeRuKeys);

        StringBuilder email = new StringBuilder();
        for (String key: allKeys) {

            String url = String.format("%s?part=%s&id=%s&key=%s", this.url, this.part, key, this.key);

            HttpGet method = new HttpGet(url);

            HttpResponse response;
            int statusCode = 0;
            String jsonContent="";
            try {
                log.debug("request to check url: {}", url);

                response = client.execute(method);
                statusCode = response.getStatusLine().getStatusCode();
                jsonContent = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                method.releaseConnection();
            }

            if (statusCode == HttpStatus.SC_OK) {
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    JsonNode jsonNode = objectMapper.readTree(jsonContent);

                    JsonNode items = jsonNode.get("items");
                    if (items.isArray()){
                        JsonNode item = items.get(0);
                        JsonNode status = item.get("status");

                        String uploadStatus = status.get("uploadStatus").asText();
                        String privacyStatus = status.get("privacyStatus").asText();
                        Boolean embeddable = status.get("embeddable").asBoolean();

                        if (!uploadStatus.equalsIgnoreCase("processed")
                            || !privacyStatus.equalsIgnoreCase("public")
                            || !embeddable) {

                            log.debug("got failed video for key: {} and content: {}", key, jsonContent);

                            generateYoutubeHeader(email);

                            List<Youtube> failedYoutubes = youtubes.stream().filter(p -> p.getUrl().contains(key)).collect(Collectors.toList());

                            for (Youtube y: failedYoutubes) {
                                String message = String.format("Youtube id = %d, name = %s, url = %s, statusCode = %s, uploadStatus = %s, privacyStatus = %s and embeddable = %b"
                                    , y.getId(), y.getName(), y.getUrl(), statusCode, uploadStatus, privacyStatus, embeddable);

                                email.append(message);
                                email.append(System.getProperty("line.separator"));
                            }

                            List<YoutubeRu> failedYoutubesRu = youtubesRu.stream().filter(p -> p.getUrl().contains(key)).collect(Collectors.toList());

                            for (YoutubeRu y: failedYoutubesRu) {
                                String message = String.format("YoutubeRu id = %d, name = %s, url = %s, statusCode = %s, uploadStatus = %s, privacyStatus = %s and embeddable = %b"
                                    , y.getId(), y.getName(), y.getUrl(), statusCode, uploadStatus, privacyStatus, embeddable);

                                email.append(message);
                                email.append(System.getProperty("line.separator"));
                            }
                        }
                    }

                } catch (IOException e) {
                    log.debug("got exception parsing for key: {} and content: {}", key, jsonContent);

                    generateYoutubeHeader(email);

                    email.append(jsonContent);
                    email.append(System.getProperty("line.separator"));

                    email.append(e.getMessage());
                    email.append(System.getProperty("line.separator"));

                    e.printStackTrace();
                }
            }else {
                log.debug("got failed url: {} and status code: {}", url, statusCode);

                generateYoutubeHeader(email);

                String message = String.format("Youtube url = %s and statusCode = %s", url, statusCode);

                email.append(message);
                email.append(System.getProperty("line.separator"));
            }
        }

        return email.toString();
    }

    /**
     * Helper to generate Youtube Header if not yet generated.
     * @param email - represent report.
     */
    private void generateYoutubeHeader(StringBuilder email) {
        if (email.length() <= 0) {
            email.append("YOUTUBES");
            email.append(System.getProperty("line.separator"));
        }
    }
}
