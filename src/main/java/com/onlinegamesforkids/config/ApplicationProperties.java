package com.onlinegamesforkids.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Online Games For Kids.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    public static class Http {
        private String port;

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }
    }

    public static class Mail {

        private String host;

        private String port;

        private String username;

        private String password;

        private String to;

        private String subject;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }
    }

    public static class YoutubeApi {

        private String url;

        private String part;

        private String key;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPart() {
            return part;
        }

        public void setPart(String part) {
            this.part = part;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    private final Http http = new Http();

    private final Mail mail = new Mail();

    private final YoutubeApi youtubeApi = new YoutubeApi();

    public Http getHttp() {
        return http;
    }

    public Mail getMail() {
        return mail;
    }

    public YoutubeApi getYoutubeApi() {
        return youtubeApi;
    }
}
