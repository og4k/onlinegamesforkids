package com.onlinegamesforkids.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.onlinegamesforkids.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Youtube.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Youtube.class.getName() + ".localizations", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.YoutubeRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.YoutubeRu.class.getName() + ".localizations", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Details.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Details.class.getName() + ".youtubeClips", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.DetailsRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.DetailsRu.class.getName() + ".youtubeClips", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Category.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Composition.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Composition.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.CompositionRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.CompositionRu.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Product.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Product.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Product.class.getName() + ".assets", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Product.class.getName() + ".localizations", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Product.class.getName() + ".details", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.CategoryRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.CategoryRu.class.getName() + ".products", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.ProductRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.ProductRu.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.ProductRu.class.getName() + ".assets", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.ProductRu.class.getName() + ".localizations", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.ProductRu.class.getName() + ".details", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.AssetType.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Asset.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Asset.class.getName() + ".type", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.AssetRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.AssetRu.class.getName() + ".type", jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Language.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.LanguageRu.class.getName(), jcacheConfiguration);
            cm.createCache(com.onlinegamesforkids.domain.Feedback.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
