package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.ProductRu;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *  Search methods for {@link ProductRu}.
 */
public interface ProductRuSearch {

    /**
     * Search for Products.
     * @param predicate - {@link Predicate}.
     * @param pageable - {@link Pageable}.
     * @return - {@link Page<ProductRu>}.
     */
    Page<ProductRu> search(Predicate predicate, Pageable pageable);
}
