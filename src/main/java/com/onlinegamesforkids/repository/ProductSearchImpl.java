package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.config.Constants;
import com.onlinegamesforkids.domain.Product;
import com.onlinegamesforkids.domain.QProduct;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * {@link ProductSearch} implementation.
 */
public class ProductSearchImpl implements ProductSearch {

    /**
     * {@link EntityManager}.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Search for Products.
     * @param predicate - {@link Predicate}.
     * @param pageable - {@link Pageable}.
     * @return - {@link Page<Product>}.
     */
    @Override
    public Page<Product> search(Predicate predicate, Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(Constants.DEFAULT_PAGE_NUMBER, Constants.DEFAULT_PAGE_SIZE, Sort.by(Sort.Direction.ASC, "id"));
        }

        QProduct product = QProduct.product;

        JPAQuery<?> query = new JPAQuery<Void>(entityManager);

        query
            .from(product)
            .leftJoin(product.assets)
            .fetchJoin()
            .leftJoin(product.localizations)
            .fetchJoin()
            .leftJoin(product.details)
            .fetchJoin()
            .where(product.enabled.isTrue(), predicate)
            .distinct();

        JPAQuery<?> queryCount = query.clone();
        long totalProducts = queryCount.fetchCount();

        PathBuilder<Product> entityPath = new PathBuilder<>(Product.class, "product");
        for (Sort.Order order : pageable.getSort()) {
            PathBuilder<Object> path = entityPath.get(order.getProperty());
            OrderSpecifier orderSpecifier = new OrderSpecifier(Order.valueOf(order.getDirection().name()), path);

            query.orderBy(orderSpecifier);
        }

        List<Product> products = query
                                    .offset(pageable.getOffset())
                                    .limit(pageable.getPageSize())
                                    .select(product)
                                    .fetch();

        return new PageImpl<>(products, pageable, totalProducts);
    }
}
