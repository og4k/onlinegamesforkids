package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.config.Constants;
import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.domain.QProductRu;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * {@link ProductRuSearch} implementation.
 */
public class ProductRuSearchImpl implements ProductRuSearch {

    /**
     * {@link EntityManager}.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Search for Products.
     * @param predicate - {@link Predicate}.
     * @param pageable - {@link Pageable}.
     * @return - {@link Page<ProductRu>}.
     */
    @Override
    public Page<ProductRu> search(Predicate predicate, Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(Constants.DEFAULT_PAGE_NUMBER, Constants.DEFAULT_PAGE_SIZE, Sort.by(Sort.Direction.ASC, "id"));
        }

        QProductRu productRu = QProductRu.productRu;

        JPAQuery<?> query = new JPAQuery<Void>(entityManager);

        query
            .from(productRu)
            .leftJoin(productRu.assets)
            .fetchJoin()
            .leftJoin(productRu.localizations)
            .fetchJoin()
            .leftJoin(productRu.details)
            .fetchJoin()
            .where(productRu.enabled.isTrue(), predicate)
            .distinct();

        JPAQuery<?> queryCount = query.clone();
        long totalProducts = queryCount.fetchCount();

        PathBuilder<ProductRu> entityPath = new PathBuilder<>(ProductRu.class, "productRu");
        for (Sort.Order order : pageable.getSort()) {
            PathBuilder<Object> path = entityPath.get(order.getProperty());
            OrderSpecifier orderSpecifier = new OrderSpecifier(Order.valueOf(order.getDirection().name()), path);

            query.orderBy(orderSpecifier);
        }

        List<ProductRu> products = query
                                    .offset(pageable.getOffset())
                                    .limit(pageable.getPageSize())
                                    .select(productRu)
                                    .fetch();

        return new PageImpl<>(products, pageable, totalProducts);
    }
}
