package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.YoutubeRu;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * YoutubeRu Repository.
 */
public interface YoutubeRuRepository extends JpaRepository<YoutubeRu, Long> {
}
