package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Product;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *  Search methods for {@link Product}.
 */
public interface ProductSearch {

    /**
     * Search for Products.
     * @param predicate - {@link Predicate}.
     * @param pageable - {@link Pageable}.
     * @return - {@link Page<Product>}.
     */
    Page<Product> search(Predicate predicate, Pageable pageable);
}
