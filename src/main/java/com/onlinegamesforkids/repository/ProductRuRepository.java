package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.domain.QProductRu;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the ProductRu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRuRepository extends JpaRepository<ProductRu, Long>,
    QuerydslPredicateExecutor<ProductRu>,
    QuerydslBinderCustomizer<QProductRu>,
    ProductRuSearch {

    @Query(value = "select distinct productRu from ProductRu productRu left join productRu.categories categoryRu left join fetch productRu.assets left join fetch productRu.localizations left join fetch productRu.details where categoryRu.id=:id and categoryRu.enabled = true and productRu.enabled = true",
        countQuery = "select count(distinct productRu) from ProductRu productRu left join productRu.categories categoryRu where categoryRu.id=:id and categoryRu.enabled = true and productRu.enabled = true")
    Page<ProductRu> findAllProductRuForOneCategoryRu(@Param("id") Long id, Pageable pageable);

    /**
     * Bring productRus by passed ids including fetching localizations and assets.
     * @param ids collection of productRu Ids {@link Set <Long>}.
     * @return {@link List <ProductRu>}.
     */
    @Query(value = "select distinct productRu from ProductRu productRu left join fetch productRu.assets left join fetch productRu.localizations left join fetch productRu.details where productRu.enabled = true and productRu.id in :ids order by productRu.id")
    List<ProductRu> findProductsRuByIds(@Param("ids") Set<Long> ids);

    /**
     * Find productRu by the passed id including fetching localizations and assets.
     * @param id productRu Id.
     * @return {@link Optional<ProductRu>}.
     */
    @Query(value = "select distinct productRu from ProductRu productRu left join fetch productRu.assets left join fetch productRu.localizations where productRu.enabled = true and productRu.id = :id")
    Optional<ProductRu> findProductRuByIdIncludeAssetsAndLocalizations(@Param("id") Long id);

    @Override
    default void customize(QuerydslBindings bindings, QProductRu root) {
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
        bindings.excluding(root.id);
        bindings.excluding(root.categories);
        bindings.excluding(root.createdBy);
        bindings.excluding(root.createdDate);
        bindings.excluding(root.description);
        bindings.excluding(root.enabled);
        bindings.excluding(root.lastModifiedBy);
        bindings.excluding(root.lastModifiedDate);
        bindings.excluding(root.localizations);
        bindings.excluding(root.url);
    }
}
