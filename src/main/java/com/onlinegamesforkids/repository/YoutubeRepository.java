package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Youtube;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Youtube Repository.
 */
public interface YoutubeRepository extends JpaRepository<Youtube, Long> {
}
