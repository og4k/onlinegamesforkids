package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.DetailsRu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * DetailsRu Repository.
 */
public interface DetailsRuRepository extends JpaRepository<DetailsRu, Long> {

    /**
     * Find DetailsRu by its Id (also fetch YoutubesRu).
     * @param id - DetailsRu Id.
     * @return found DetailsRu or null {@link Optional<DetailsRu>}.
     */
    @Override
    @Query(value = "select distinct detailsRu from DetailsRu detailsRu left join fetch detailsRu.youtubeClips where detailsRu.id = :id")
    Optional<DetailsRu> findById(Long id);
}
