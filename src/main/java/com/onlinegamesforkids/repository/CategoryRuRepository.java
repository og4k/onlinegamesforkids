package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.CategoryRu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the CategoryRu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRuRepository extends JpaRepository<CategoryRu, Long> {

    @Query(value = "select distinct category_ru from CategoryRu category_ru left join fetch category_ru.products where category_ru.enabled = true order by category_ru.id",
        countQuery = "select count(distinct category_ru) from CategoryRu category_ru where category_ru.enabled = true")
    Page<CategoryRu> findAllWithEagerRelationships(Pageable pageable);

    @Override
    @Query(value = "select distinct category_ru from CategoryRu category_ru where category_ru.enabled = true order by category_ru.id",
        countQuery = "select count(distinct category_ru) from CategoryRu category_ru where category_ru.enabled = true")
    Page<CategoryRu> findAll(Pageable pageable);

    @Query(value = "select distinct category_ru from CategoryRu category_ru left join fetch category_ru.products where category_ru.enabled = true order by category_ru.id")
    List<CategoryRu> findAllWithEagerRelationships();

    @Query("select category_ru from CategoryRu category_ru left join fetch category_ru.products where category_ru.id = :id and category_ru.enabled = true order by category_ru.id")
    Optional<CategoryRu> findOneWithEagerRelationships(@Param("id") Long id);

}
