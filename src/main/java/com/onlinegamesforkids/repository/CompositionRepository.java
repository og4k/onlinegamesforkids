package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Composition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * Composition Repository.
 */
public interface CompositionRepository extends JpaRepository<Composition, Long> {

    /**
     * Find only enabled Composition by its Id (also fetch Products).
     * @param id - Composition Id.
     * @return found Composition or null {@link Optional<Composition>}.
     */
    @Override
    @Query(value = "select distinct composition from Composition composition left join fetch composition.products where composition.id = :id and composition.enabled = true")
    Optional<Composition> findById(Long id);
}
