package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
}
