package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.CompositionRu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * CompositionRu Repository.
 */
public interface CompositionRuRepository extends JpaRepository<CompositionRu, Long> {

    /**
     * Find only enabled CompositionRu by its Id (also fetch ProductRu).
     * @param id - CompositionRu Id.
     * @return found CompositionRu or null {@link Optional<CompositionRu>}.
     */
    @Override
    @Query(value = "select distinct compositionRu from CompositionRu compositionRu left join fetch compositionRu.products where compositionRu.id = :id and compositionRu.enabled = true")
    Optional<CompositionRu> findById(Long id);
}
