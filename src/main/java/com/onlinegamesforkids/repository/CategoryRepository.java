package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "select distinct category from Category category left join fetch category.products where category.enabled = true order by category.id",
        countQuery = "select count(distinct category) from Category category where category.enabled = true")
    Page<Category> findAllWithEagerRelationships(Pageable pageable);

    @Override
    @Query(value = "select distinct category from Category category where category.enabled = true order by category.id",
        countQuery = "select count(distinct category) from Category category where category.enabled = true")
    Page<Category> findAll(Pageable pageable);

    @Query(value = "select distinct category from Category category left join fetch category.products where category.enabled = true order by category.id")
    List<Category> findAllWithEagerRelationships();

    @Query("select category from Category category left join fetch category.products where category.id = :id and category.enabled = true order by category.id")
    Optional<Category> findOneWithEagerRelationships(@Param("id") Long id);

}
