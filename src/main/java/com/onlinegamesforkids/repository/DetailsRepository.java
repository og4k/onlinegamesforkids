package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Details;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * Details Repository.
 */
public interface DetailsRepository extends JpaRepository<Details, Long> {

    /**
     * Find Details by its Id (also fetch Youtubes).
     * @param id - Details Id.
     * @return found Details or null {@link Optional<Details>}.
     */
    @Override
    @Query(value = "select distinct details from Details details left join fetch details.youtubeClips where details.id = :id")
    Optional<Details> findById(Long id);
}
