package com.onlinegamesforkids.repository;

import com.onlinegamesforkids.domain.Product;
import com.onlinegamesforkids.domain.QProduct;
import com.querydsl.core.types.dsl.StringExpression;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import com.querydsl.core.types.dsl.StringPath;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>,
    QuerydslPredicateExecutor<Product>,
    QuerydslBinderCustomizer<QProduct>,
    ProductSearch {

    @Query(value = "select distinct product from Product product left join product.categories category left join fetch product.assets left join fetch product.localizations left join fetch product.details where category.id = :id and category.enabled = true and product.enabled = true",
        countQuery = "select count(distinct product) from Product product left join product.categories category where category.id=:id and category.enabled = true and product.enabled = true")
    Page<Product> findAllProductsForOneCategory(@Param("id") Long id, Pageable pageable);

    /**
     * Bring products by passed ids including fetching localizations and assets.
     * @param ids collection of product Ids {@link Set<Long>}.
     * @return {@link List<Product>}.
     */
    @Query(value = "select distinct product from Product product left join fetch product.assets left join fetch product.localizations left join fetch product.details where product.enabled = true and product.id in :ids order by product.id")
    List<Product> findProductsByIds(@Param("ids") Set<Long> ids);

    /**
     * Find product by the passed id including fetching localizations and assets.
     * @param id product Id.
     * @return {@link Optional<Product>}.
     */
    @Query(value = "select distinct product from Product product left join fetch product.assets left join fetch product.localizations where product.enabled = true and product.id = :id")
    Optional<Product> findProductByIdIncludeAssetsAndLocalizations(@Param("id") Long id);

    @Override
    default void customize(QuerydslBindings bindings, QProduct root) {
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
        bindings.excluding(root.id);
        bindings.excluding(root.categories);
        bindings.excluding(root.createdBy);
        bindings.excluding(root.createdDate);
        bindings.excluding(root.description);
        bindings.excluding(root.enabled);
        bindings.excluding(root.lastModifiedBy);
        bindings.excluding(root.lastModifiedDate);
        bindings.excluding(root.localizations);
        bindings.excluding(root.url);
    }
}
