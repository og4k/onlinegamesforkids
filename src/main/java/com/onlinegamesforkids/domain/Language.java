package com.onlinegamesforkids.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 *  Language.
 */
@Entity
@Table(name = "language")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Language extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull
    @Size(min = 2, max = 2)
    @Column(name = "two_letter_name", length = 2, nullable = false)
    private String twoLetterName;

    @NotNull
    @Size(min = 3, max = 3)
    @Column(name = "three_letter_name", length = 3, nullable = false)
    private String threeLetterName;

    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTwoLetterName() {
        return twoLetterName;
    }

    public void setTwoLetterName(String twoLetterName) {
        this.twoLetterName = twoLetterName;
    }

    public String getThreeLetterName() {
        return threeLetterName;
    }

    public void setThreeLetterName(String threeLetterName) {
        this.threeLetterName = threeLetterName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Language language = (Language) o;
        if (language.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), language.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Language{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", two_letter_name='" + getTwoLetterName() + "'" +
            ", three_letter_name='" + getThreeLetterName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
