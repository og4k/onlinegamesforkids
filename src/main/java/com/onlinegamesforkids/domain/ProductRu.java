package com.onlinegamesforkids.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ProductRu.
 */
@Entity
@Table(name = "product_ru")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProductRu extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    /**
     * Url.
     * It might contain information such as width and height separated by pipe ( we use it to embed 4j.com).
     */
    @NotNull
    @Size(max = 256)
    @Column(name = "url", length = 256, nullable = false)
    private String url;

    @OneToMany
    @JoinTable(name = "product_ru_asset_ru",
        joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "asset_id", referencedColumnName = "id"))
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<AssetRu> assets = new HashSet<>();

    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @OneToMany
    @JoinTable(name = "product_ru_language_ru",
        joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "language_id", referencedColumnName = "id"))
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<LanguageRu> localizations = new HashSet<>();

    @ManyToMany(mappedBy = "products")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<CategoryRu> categories = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "details_id")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private DetailsRu details;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ProductRu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public ProductRu description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public ProductRu url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<AssetRu> getAssets() {
        return assets;
    }

    public void setAssets(Set<AssetRu> assets) {
        this.assets = assets;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public ProductRu enabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<LanguageRu> getLocalizations() {
        return localizations;
    }

    public void setLocalizations(Set<LanguageRu> localizations) {
        this.localizations = localizations;
    }

    public Set<CategoryRu> getCategories() {
        return categories;
    }

    public ProductRu categories(Set<CategoryRu> categoryRus) {
        this.categories = categoryRus;
        return this;
    }

    public ProductRu addCategory(CategoryRu categoryRu) {
        this.categories.add(categoryRu);
        categoryRu.getProducts().add(this);
        return this;
    }

    public ProductRu removeCategory(CategoryRu categoryRu) {
        this.categories.remove(categoryRu);
        categoryRu.getProducts().remove(this);
        return this;
    }

    public void setCategories(Set<CategoryRu> categoryRus) {
        this.categories = categoryRus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public DetailsRu getDetails() {
        return details;
    }

    public void setDetails(DetailsRu details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductRu productRu = (ProductRu) o;
        if (productRu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productRu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductRu{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", url='" + getUrl() + "'" +
            ", enabled='" + isEnabled() + "'" +
            "}";
    }
}
