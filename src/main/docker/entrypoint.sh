#!/bin/sh

echo "The application will start in ${JHIPSTER_SLEEP}s..." && sleep ${JHIPSTER_SLEEP}
exec java ${JAVA_OPTS} -Djdk.tls.ephemeralDHKeySize=2048 -Djava.security.egd=file:/dev/./urandom -jar "${HOME}/app.war" "$@"

#-Djdk.tls.ephemeralDHKeySize=2048
