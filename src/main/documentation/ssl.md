# SSL Configuration

We use [cheapsslsecurity.com][] to obtain a SSL certificate. Firstly we have to generate a CSR (we use their instructions for Tomcat).

## CSR Generation Instructions

* Go to the **Directory** where you will manage the certificate.
* Generate a **keystore** and **private key** by running the following command:


    keytool -genkey -alias server -keyalg RSA -keysize 2048 -keystore your_domain_name.jks

**Note**: *Replace "your_domain_name" with the primary domain you will be securing with the certificate.*

* Enter a **keystore password**.
* Enter your **Organization Information**.
* **Note**: *When prompted to enter your First and Last Name, input your “domain name” instead of your personal name.*
* Enter **y** or **yes** when prompted only if all information is correct.
* Enter your **keystore password** and press **Enter**.
* Your **keystore** has been created in the current directory.
* From the newly created keystore, generate your **CSR** by running the following **keytool command**:


    keytool -certreq -alias server -file csr.txt -keystore your_domain_name.jks

* Enter your **keystore password** and press **Enter**.
* Your **CSR** has been created in the current directory.
* Locate and open the newly created CSR in a text editor such as Notepad and copy all the text including:
-----BEGIN CERTIFICATE REQUEST-----
And
-----END CERTIFICATE REQUEST-----

* Return to the **Generation Form** on our website and paste the entire CSR into the blank text box and continue with completing the generation process.

Once successfully validated you will receive an email containing the following files:

Attached to this email you should find a .zip file containing:

* **Root CA Certificate** - AddTrustExternalCARoot.crt
* **Intermediate CA Certificate** - COMODORSAAddTrustCA.crt
* **Intermediate CA Certificate** - COMODORSADomainValidationSecureServerCA.crt
* **Your PositiveSSL Certificate** - kidsgoat_com.crt

## Generating P12

Now we have to generate a p12 certificate format used on our spring boot server.

* We have to export a private key into a keystore by running the following **keytool command**:

    
    keytool -importkeystore -srckeystore kidsgoat.jks -destkeystore keystore.p12 -deststoretype PKCS12
    
* Next we export the private key into unencrypted file by running the following **keytool command**:  


    openssl pkcs12 -in keystore.p12  -nodes -nocerts -out kidsgoat_com.key
    
    
* Finally we generate a p12 file used in our application by running the following **keytool command**:  


    cat COMODORSADomainValidationSecureServerCA.crt COMODORSAAddTrustCA.crt > bundle.crt 
    openssl pkcs12 -export -inkey kidsgoat_com.key -in kidsgoat_com.crt -certfile bundle.crt -out keystore.p12 -name server
    
 
 ## Useful commands
 
To check validity of private key, certificate and csr by running the following **keytool commands**:  
 
 
     openssl pkey -in kidsgoat_com.key -pubout -outform pem | shasum -a 256
     dffa4d822332e22e5e89a5518935d7dd1853b3343c08e5a8fb70b8af01462f50  -
     
     openssl x509 -in kidsgoat_com.crt -pubkey -noout -outform pem | shasum -a 256
     dffa4d822332e22e5e89a5518935d7dd1853b3343c08e5a8fb70b8af01462f50  -
     
     openssl req -in csr.txt -pubkey -noout -outform pem | shasum -a 256
     dffa4d822332e22e5e89a5518935d7dd1853b3343c08e5a8fb70b8af01462f50  -
    
You can see that each of them produces exactly the same hash.

To verify certificate chain use the following **keytool command**:  


    openssl verify -CAfile <(cat AddTrustExternalCARoot.crt COMODORSAAddTrustCA.crt COMODORSADomainValidationSecureServerCA.crt) kidsgoat_com.crt
    
[cheapsslsecurity.com]: https://cheapsslsecurity.com/

