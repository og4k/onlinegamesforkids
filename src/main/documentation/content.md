# Adding Content

## OG4K_Assets Repository

We have a separate project called og4k_asserts where under the directory of products we add new products. Each has its own
folder and each contains one png image and one mp4 video clip. The image has to be less than 100 kb in size and has a resolution 
of 530*300 px. Each video clip is less or equal 10s and less than 800kb.

I suggest to use [compresspng.com][] to reduce images size.
I suggest to use [handbrake.fr][] to reduce video clips size.

Very often we need to make a video clip consisting of many small parts. For this purposes we can use iMovie or
similar software.

As an example we use the game of Sonic Hedgehog. Pay attention to names: we use underscore (_) as a separator,
if names are longer than one word. 

## Database Update

Once we have our resources in the og4k_assets repository we can upload then onto our S3 bucket which is in turn uses
the CloudFront service for CDN.

To upload assets onto S3 Service we can use aws s3 cli.

    aws s3 cp og4k_assets/products s3://og4k-assets/products --recursive --exclude ".DS_Store"

Now we have to add these resources into our assets.csv and assets_ru.csv files and add mappings between new resources and 
products in the product_asset_mappings.csv and product_ru_asset_ru_mappings.csv.

To synchronize assets with S3 Service we can use aws s3 cli.

    aws s3 sync products s3://og4k-assets/products --exclude "*.DS_Store*"
    
**Don't forget to make your assets public!!!**

[compresspng.com]: https://compresspng.com/
[handbrake.fr]: https://handbrake.fr/
