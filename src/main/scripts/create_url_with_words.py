import sys
import requests
import shutil
import os

# Run python with create_url_with_words.py filename_1(products.csv) filename_2(file to use for check)

urls_to = []
num_lines = 1

filename_1 = sys.argv[1]
filename_2 = sys.argv[2]

file_from = open(filename_1, "r", encoding="utf-8")
file_to = open(filename_2, "a", encoding="utf-8")

if os.stat(filename_2).st_size:
    num_lines = sum(1 for line in file_to)
    urls_to = [i.split(";")[2] for i in file_to]
urls_from = [i.split(";")[3] for i in file_from][1:]


for i in urls_from:
    if i not in urls_to:
        file_to.write(str(num_lines) + ";" + i + ";\n")
    num_lines += 1

file_to.close()
file_from.close()
