import sys, requests, time, smtplib, datetime, sched

# To use script start with python 3.6 with input csv file (path_to_python/python.exe validate_content.py out.csv)

timeout_for_check = 21600


def send_email(title, msg):
    server = smtplib.SMTP_SSL('smtp.yandex.com.tr', 465)
    server.login("testmein", "QWEpoi3$")
    message = 'Subject: {}\n\n{}'.format(title, msg)
    server.sendmail("testmein@yandex.ru", "testmein@yandex.ru", message)
    server.quit()


def get_content(url):
    # Get content and natural exception if link is broken
    try:
        return requests.get(url).text
    except:
        return False


def check_conent_(content, words):
    # Check that all words are present on html page
    sum = len(words)
    x = 0
    for i in words:
        if content.find(i) is not -1:
            x += 1
    if x == sum:
        return True
    return False


def periodic(scheduler, interval, action, actionargs=()):
    # Scheduler to run check periodically and indefinitely
    scheduler.enter(interval, 1, periodic,
                    (scheduler, interval, action, actionargs))
    action(*actionargs)


def main():
    # Tags, class names etc that can define game should be added to this list

    timeout_between_runs = 600

    list_failed, list_aborted = [], []
    msg = ""
    date_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

    # Open csv file
    filename = sys.argv[1]
    with open(filename) as f:
        content = f.readlines()
    f.close()

    dic = {}
    j = 0
    urls = [i.split(";")[1] for i in content]
    words = [i.split(";")[2] for i in content]
    for i in urls:
        dic[i] = words[j]
        j += 1

    # Check to find failed and broken link tests
    for x in range(5):
        if x == 0:
            list_to_check = urls
        else:
            list_to_check = sorted(set(list_failed + list_aborted))
        list_failed, list_aborted = [], []
        for i in list_to_check:
            url_content = get_content(i)
            if not url_content:
                list_aborted.append(i)
            if url_content:
                if not check_conent_(url_content, dic[i].split(",")):
                    list_failed.append(i)

    time.sleep(timeout_between_runs)

    # Creating message to send in email
    msg += "\n Links that broken:\n"
    for i in list_aborted:
        msg += str(i) + "\n"

    msg += "\n Games not found:\n"
    for i in list_failed:
        msg += str(i) + "\n"

    send_email("Games validation results on " + str(date_time), msg)


if __name__ == "__main__":
    scheduler = sched.scheduler(time.time, time.sleep)
    periodic(scheduler, timeout_for_check, main, ())

    scheduler.run()
