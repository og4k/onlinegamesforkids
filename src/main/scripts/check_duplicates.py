import csv

#----------------------------------------------------------------------
def csv_dict_reader(file_obj, separator):
    """
    Read a CSV file using csv.DictReader
    """
    reader = csv.DictReader(file_obj, delimiter=separator)
    for line in reader:
        print("{0} - {1}".format(line["name"], line["url"]))
#----------------------------------------------------------------------

#----------------------------------------------------------------------
def csv_check_duplicates(file_obj, separator):
    """
    Read a CSV file using csv.DictReader
    """
    reader = csv.DictReader(file_obj, delimiter=separator)
    duplicates = {}
    for line in reader:
        word = line["name"].lower().strip()
        if word in duplicates:
            duplicates[word] += 1
        else:
            duplicates[word] = 1

    for k, v in duplicates.items():
        if v > 1:
            print("we have {0} duplicates for '{1}'".format(v, k))

    print("**DONE**")

#----------------------------------------------------------------------

if __name__ == "__main__":
    with open("../resources/config/liquibase/products.csv") as f_obj:
        separator = ';'
        csv_dict_reader(f_obj, separator)
        csv_check_duplicates(f_obj, separator)
