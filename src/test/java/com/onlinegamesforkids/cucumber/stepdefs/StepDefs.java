package com.onlinegamesforkids.cucumber.stepdefs;

import com.onlinegamesforkids.OnlineGamesForKidsApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = OnlineGamesForKidsApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
