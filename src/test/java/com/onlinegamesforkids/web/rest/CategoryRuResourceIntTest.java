package com.onlinegamesforkids.web.rest;

import com.onlinegamesforkids.OnlineGamesForKidsApp;

import com.onlinegamesforkids.domain.CategoryRu;
import com.onlinegamesforkids.repository.CategoryRuRepository;
import com.onlinegamesforkids.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static com.onlinegamesforkids.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CategoryRuResource REST controller.
 *
 * @see CategoryRuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OnlineGamesForKidsApp.class)
public class CategoryRuResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLED = true;
    private static final Boolean UPDATED_ENABLED = true;

    @Autowired
    private CategoryRuRepository categoryRuRepository;
    @Mock
    private CategoryRuRepository categoryRuRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCategoryRuMockMvc;

    private CategoryRu categoryRu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CategoryRuResource categoryRuResource = new CategoryRuResource(categoryRuRepository);
        this.restCategoryRuMockMvc = MockMvcBuilders.standaloneSetup(categoryRuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryRu createEntity(EntityManager em) {
        CategoryRu categoryRu = new CategoryRu()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .enabled(DEFAULT_ENABLED);
        return categoryRu;
    }

    @Before
    public void initTest() {
        categoryRu = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategoryRu() throws Exception {
        int databaseSizeBeforeCreate = categoryRuRepository.findAll().size();

        // Create the CategoryRu
        restCategoryRuMockMvc.perform(post("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryRu)))
            .andExpect(status().isCreated());

        // Validate the CategoryRu in the database
        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeCreate + 1);
        CategoryRu testCategoryRu = categoryRuList.get(categoryRuList.size() - 1);
        assertThat(testCategoryRu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCategoryRu.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCategoryRu.isEnabled()).isEqualTo(DEFAULT_ENABLED);
    }

    @Test
    @Transactional
    public void createCategoryRuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoryRuRepository.findAll().size();

        // Create the CategoryRu with an existing ID
        categoryRu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoryRuMockMvc.perform(post("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryRu)))
            .andExpect(status().isBadRequest());

        // Validate the CategoryRu in the database
        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryRuRepository.findAll().size();
        // set the field null
        categoryRu.setName(null);

        // Create the CategoryRu, which fails.

        restCategoryRuMockMvc.perform(post("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryRu)))
            .andExpect(status().isBadRequest());

        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryRuRepository.findAll().size();
        // set the field null
        categoryRu.setEnabled(null);

        // Create the CategoryRu, which fails.

        restCategoryRuMockMvc.perform(post("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryRu)))
            .andExpect(status().isBadRequest());

        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCategoryRus() throws Exception {
        // Initialize the database
        categoryRuRepository.saveAndFlush(categoryRu);

        // Get all the categoryRuList
        restCategoryRuMockMvc.perform(get("/api/category-rus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoryRu.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())));
    }

    public void getAllCategoryRusWithEagerRelationshipsIsEnabled() throws Exception {
        CategoryRuResource categoryRuResource = new CategoryRuResource(categoryRuRepositoryMock);
        when(categoryRuRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restCategoryRuMockMvc = MockMvcBuilders.standaloneSetup(categoryRuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restCategoryRuMockMvc.perform(get("/api/category-rus?eagerload=true"))
        .andExpect(status().isOk());

        verify(categoryRuRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    public void getAllCategoryRusWithEagerRelationshipsIsNotEnabled() throws Exception {
        CategoryRuResource categoryRuResource = new CategoryRuResource(categoryRuRepositoryMock);
            when(categoryRuRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restCategoryRuMockMvc = MockMvcBuilders.standaloneSetup(categoryRuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restCategoryRuMockMvc.perform(get("/api/category-rus?eagerload=true"))
        .andExpect(status().isOk());

            verify(categoryRuRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getCategoryRu() throws Exception {
        // Initialize the database
        categoryRuRepository.saveAndFlush(categoryRu);

        // Get the categoryRu
        restCategoryRuMockMvc.perform(get("/api/category-rus/{id}", categoryRu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(categoryRu.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCategoryRu() throws Exception {
        // Get the categoryRu
        restCategoryRuMockMvc.perform(get("/api/category-rus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategoryRu() throws Exception {
        // Initialize the database
        categoryRuRepository.saveAndFlush(categoryRu);

        int databaseSizeBeforeUpdate = categoryRuRepository.findAll().size();

        // Update the categoryRu
        CategoryRu updatedCategoryRu = categoryRuRepository.findById(categoryRu.getId()).get();
        // Disconnect from session so that the updates on updatedCategoryRu are not directly saved in db
        em.detach(updatedCategoryRu);
        updatedCategoryRu
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .enabled(UPDATED_ENABLED);

        restCategoryRuMockMvc.perform(put("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCategoryRu)))
            .andExpect(status().isOk());

        // Validate the CategoryRu in the database
        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeUpdate);
        CategoryRu testCategoryRu = categoryRuList.get(categoryRuList.size() - 1);
        assertThat(testCategoryRu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCategoryRu.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCategoryRu.isEnabled()).isEqualTo(UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void updateNonExistingCategoryRu() throws Exception {
        int databaseSizeBeforeUpdate = categoryRuRepository.findAll().size();

        // Create the CategoryRu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCategoryRuMockMvc.perform(put("/api/category-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryRu)))
            .andExpect(status().isBadRequest());

        // Validate the CategoryRu in the database
        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCategoryRu() throws Exception {
        // Initialize the database
        categoryRuRepository.saveAndFlush(categoryRu);

        int databaseSizeBeforeDelete = categoryRuRepository.findAll().size();

        // Get the categoryRu
        restCategoryRuMockMvc.perform(delete("/api/category-rus/{id}", categoryRu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CategoryRu> categoryRuList = categoryRuRepository.findAll();
        assertThat(categoryRuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoryRu.class);
        CategoryRu categoryRu1 = new CategoryRu();
        categoryRu1.setId(1L);
        CategoryRu categoryRu2 = new CategoryRu();
        categoryRu2.setId(categoryRu1.getId());
        assertThat(categoryRu1).isEqualTo(categoryRu2);
        categoryRu2.setId(2L);
        assertThat(categoryRu1).isNotEqualTo(categoryRu2);
        categoryRu1.setId(null);
        assertThat(categoryRu1).isNotEqualTo(categoryRu2);
    }
}
