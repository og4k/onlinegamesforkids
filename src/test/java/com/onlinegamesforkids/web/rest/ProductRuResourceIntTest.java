package com.onlinegamesforkids.web.rest;

import com.onlinegamesforkids.OnlineGamesForKidsApp;

import com.onlinegamesforkids.domain.ProductRu;
import com.onlinegamesforkids.repository.ProductRuRepository;
import com.onlinegamesforkids.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.onlinegamesforkids.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProductRuResource REST controller.
 *
 * @see ProductRuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OnlineGamesForKidsApp.class)
public class ProductRuResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLED = false;
    private static final Boolean UPDATED_ENABLED = true;

    @Autowired
    private ProductRuRepository productRuRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProductRuMockMvc;

    private ProductRu productRu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductRuResource productRuResource = new ProductRuResource(productRuRepository);
        this.restProductRuMockMvc = MockMvcBuilders.standaloneSetup(productRuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductRu createEntity(EntityManager em) {
        ProductRu productRu = new ProductRu()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .url(DEFAULT_URL)
            .enabled(DEFAULT_ENABLED);
        return productRu;
    }

    @Before
    public void initTest() {
        productRu = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductRu() throws Exception {
        int databaseSizeBeforeCreate = productRuRepository.findAll().size();

        // Create the ProductRu
        restProductRuMockMvc.perform(post("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isCreated());

        // Validate the ProductRu in the database
        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeCreate + 1);
        ProductRu testProductRu = productRuList.get(productRuList.size() - 1);
        assertThat(testProductRu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProductRu.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProductRu.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testProductRu.isEnabled()).isEqualTo(DEFAULT_ENABLED);
    }

    @Test
    @Transactional
    public void createProductRuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productRuRepository.findAll().size();

        // Create the ProductRu with an existing ID
        productRu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductRuMockMvc.perform(post("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isBadRequest());

        // Validate the ProductRu in the database
        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRuRepository.findAll().size();
        // set the field null
        productRu.setName(null);

        // Create the ProductRu, which fails.

        restProductRuMockMvc.perform(post("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isBadRequest());

        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRuRepository.findAll().size();
        // set the field null
        productRu.setUrl(null);

        // Create the ProductRu, which fails.

        restProductRuMockMvc.perform(post("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isBadRequest());

        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnabledIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRuRepository.findAll().size();
        // set the field null
        productRu.setEnabled(null);

        // Create the ProductRu, which fails.

        restProductRuMockMvc.perform(post("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isBadRequest());

        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductRus() throws Exception {
        // Initialize the database
        productRuRepository.saveAndFlush(productRu);

        // Get all the productRuList
        restProductRuMockMvc.perform(get("/api/product-rus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productRu.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].enabled").value(hasItem(DEFAULT_ENABLED.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getProductRu() throws Exception {
        // Initialize the database
        productRuRepository.saveAndFlush(productRu);

        // Get the productRu
        restProductRuMockMvc.perform(get("/api/product-rus/{id}", productRu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(productRu.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.enabled").value(DEFAULT_ENABLED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProductRu() throws Exception {
        // Get the productRu
        restProductRuMockMvc.perform(get("/api/product-rus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductRu() throws Exception {
        // Initialize the database
        productRuRepository.saveAndFlush(productRu);

        int databaseSizeBeforeUpdate = productRuRepository.findAll().size();

        // Update the productRu
        ProductRu updatedProductRu = productRuRepository.findById(productRu.getId()).get();
        // Disconnect from session so that the updates on updatedProductRu are not directly saved in db
        em.detach(updatedProductRu);
        updatedProductRu
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .url(UPDATED_URL)
            .enabled(UPDATED_ENABLED);

        restProductRuMockMvc.perform(put("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductRu)))
            .andExpect(status().isOk());

        // Validate the ProductRu in the database
        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeUpdate);
        ProductRu testProductRu = productRuList.get(productRuList.size() - 1);
        assertThat(testProductRu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductRu.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProductRu.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testProductRu.isEnabled()).isEqualTo(UPDATED_ENABLED);
    }

    @Test
    @Transactional
    public void updateNonExistingProductRu() throws Exception {
        int databaseSizeBeforeUpdate = productRuRepository.findAll().size();

        // Create the ProductRu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProductRuMockMvc.perform(put("/api/product-rus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRu)))
            .andExpect(status().isBadRequest());

        // Validate the ProductRu in the database
        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductRu() throws Exception {
        // Initialize the database
        productRuRepository.saveAndFlush(productRu);

        int databaseSizeBeforeDelete = productRuRepository.findAll().size();

        // Get the productRu
        restProductRuMockMvc.perform(delete("/api/product-rus/{id}", productRu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductRu> productRuList = productRuRepository.findAll();
        assertThat(productRuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductRu.class);
        ProductRu productRu1 = new ProductRu();
        productRu1.setId(1L);
        ProductRu productRu2 = new ProductRu();
        productRu2.setId(productRu1.getId());
        assertThat(productRu1).isEqualTo(productRu2);
        productRu2.setId(2L);
        assertThat(productRu1).isNotEqualTo(productRu2);
        productRu1.setId(null);
        assertThat(productRu1).isNotEqualTo(productRu2);
    }
}
