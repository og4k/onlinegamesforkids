package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProductRu is a Querydsl query type for ProductRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProductRu extends EntityPathBase<ProductRu> {

    private static final long serialVersionUID = 1657064289L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProductRu productRu = new QProductRu("productRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    public final SetPath<AssetRu, QAssetRu> assets = this.<AssetRu, QAssetRu>createSet("assets", AssetRu.class, QAssetRu.class, PathInits.DIRECT2);

    public final SetPath<CategoryRu, QCategoryRu> categories = this.<CategoryRu, QCategoryRu>createSet("categories", CategoryRu.class, QCategoryRu.class, PathInits.DIRECT2);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final QDetailsRu details;

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final SetPath<LanguageRu, QLanguageRu> localizations = this.<LanguageRu, QLanguageRu>createSet("localizations", LanguageRu.class, QLanguageRu.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final StringPath url = createString("url");

    public QProductRu(String variable) {
        this(ProductRu.class, forVariable(variable), INITS);
    }

    public QProductRu(Path<? extends ProductRu> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProductRu(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProductRu(PathMetadata metadata, PathInits inits) {
        this(ProductRu.class, metadata, inits);
    }

    public QProductRu(Class<? extends ProductRu> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.details = inits.isInitialized("details") ? new QDetailsRu(forProperty("details")) : null;
    }

}

