package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QYoutube is a Querydsl query type for Youtube
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QYoutube extends EntityPathBase<Youtube> {

    private static final long serialVersionUID = 1571964594L;

    public static final QYoutube youtube = new QYoutube("youtube");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final SetPath<Language, QLanguage> localizations = this.<Language, QLanguage>createSet("localizations", Language.class, QLanguage.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final StringPath url = createString("url");

    public QYoutube(String variable) {
        super(Youtube.class, forVariable(variable));
    }

    public QYoutube(Path<? extends Youtube> path) {
        super(path.getType(), path.getMetadata());
    }

    public QYoutube(PathMetadata metadata) {
        super(Youtube.class, metadata);
    }

}

