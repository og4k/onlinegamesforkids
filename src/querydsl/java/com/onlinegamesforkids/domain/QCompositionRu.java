package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCompositionRu is a Querydsl query type for CompositionRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCompositionRu extends EntityPathBase<CompositionRu> {

    private static final long serialVersionUID = -1597159972L;

    public static final QCompositionRu compositionRu = new QCompositionRu("compositionRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final SetPath<ProductRu, QProductRu> products = this.<ProductRu, QProductRu>createSet("products", ProductRu.class, QProductRu.class, PathInits.DIRECT2);

    public QCompositionRu(String variable) {
        super(CompositionRu.class, forVariable(variable));
    }

    public QCompositionRu(Path<? extends CompositionRu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCompositionRu(PathMetadata metadata) {
        super(CompositionRu.class, metadata);
    }

}

