package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDetails is a Querydsl query type for Details
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDetails extends EntityPathBase<Details> {

    private static final long serialVersionUID = -173535791L;

    public static final QDetails details = new QDetails("details");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final SetPath<Youtube, QYoutube> youtubeClips = this.<Youtube, QYoutube>createSet("youtubeClips", Youtube.class, QYoutube.class, PathInits.DIRECT2);

    public QDetails(String variable) {
        super(Details.class, forVariable(variable));
    }

    public QDetails(Path<? extends Details> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDetails(PathMetadata metadata) {
        super(Details.class, metadata);
    }

}

