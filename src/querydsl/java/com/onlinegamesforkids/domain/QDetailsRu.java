package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QDetailsRu is a Querydsl query type for DetailsRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDetailsRu extends EntityPathBase<DetailsRu> {

    private static final long serialVersionUID = 735832052L;

    public static final QDetailsRu detailsRu = new QDetailsRu("detailsRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final SetPath<YoutubeRu, QYoutubeRu> youtubeClips = this.<YoutubeRu, QYoutubeRu>createSet("youtubeClips", YoutubeRu.class, QYoutubeRu.class, PathInits.DIRECT2);

    public QDetailsRu(String variable) {
        super(DetailsRu.class, forVariable(variable));
    }

    public QDetailsRu(Path<? extends DetailsRu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDetailsRu(PathMetadata metadata) {
        super(DetailsRu.class, metadata);
    }

}

