package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QComposition is a Querydsl query type for Composition
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QComposition extends EntityPathBase<Composition> {

    private static final long serialVersionUID = -341326407L;

    public static final QComposition composition = new QComposition("composition");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final SetPath<Product, QProduct> products = this.<Product, QProduct>createSet("products", Product.class, QProduct.class, PathInits.DIRECT2);

    public QComposition(String variable) {
        super(Composition.class, forVariable(variable));
    }

    public QComposition(Path<? extends Composition> path) {
        super(path.getType(), path.getMetadata());
    }

    public QComposition(PathMetadata metadata) {
        super(Composition.class, metadata);
    }

}

