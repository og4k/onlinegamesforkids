package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCategoryRu is a Querydsl query type for CategoryRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCategoryRu extends EntityPathBase<CategoryRu> {

    private static final long serialVersionUID = -627079694L;

    public static final QCategoryRu categoryRu = new QCategoryRu("categoryRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final SetPath<ProductRu, QProductRu> products = this.<ProductRu, QProductRu>createSet("products", ProductRu.class, QProductRu.class, PathInits.DIRECT2);

    public QCategoryRu(String variable) {
        super(CategoryRu.class, forVariable(variable));
    }

    public QCategoryRu(Path<? extends CategoryRu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCategoryRu(PathMetadata metadata) {
        super(CategoryRu.class, metadata);
    }

}

