package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAssetRu is a Querydsl query type for AssetRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAssetRu extends EntityPathBase<AssetRu> {

    private static final long serialVersionUID = 1858933986L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAssetRu assetRu = new QAssetRu("assetRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath location = createString("location");

    public final StringPath name = createString("name");

    public final QAssetType type;

    public QAssetRu(String variable) {
        this(AssetRu.class, forVariable(variable), INITS);
    }

    public QAssetRu(Path<? extends AssetRu> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAssetRu(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAssetRu(PathMetadata metadata, PathInits inits) {
        this(AssetRu.class, metadata, inits);
    }

    public QAssetRu(Class<? extends AssetRu> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.type = inits.isInitialized("type") ? new QAssetType(forProperty("type")) : null;
    }

}

