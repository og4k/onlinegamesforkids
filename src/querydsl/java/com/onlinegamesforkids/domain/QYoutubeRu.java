package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QYoutubeRu is a Querydsl query type for YoutubeRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QYoutubeRu extends EntityPathBase<YoutubeRu> {

    private static final long serialVersionUID = -1170510699L;

    public static final QYoutubeRu youtubeRu = new QYoutubeRu("youtubeRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final SetPath<LanguageRu, QLanguageRu> localizations = this.<LanguageRu, QLanguageRu>createSet("localizations", LanguageRu.class, QLanguageRu.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final StringPath url = createString("url");

    public QYoutubeRu(String variable) {
        super(YoutubeRu.class, forVariable(variable));
    }

    public QYoutubeRu(Path<? extends YoutubeRu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QYoutubeRu(PathMetadata metadata) {
        super(YoutubeRu.class, metadata);
    }

}

