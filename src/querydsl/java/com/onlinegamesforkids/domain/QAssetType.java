package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAssetType is a Querydsl query type for AssetType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAssetType extends EntityPathBase<AssetType> {

    private static final long serialVersionUID = -270767591L;

    public static final QAssetType assetType = new QAssetType("assetType");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public QAssetType(String variable) {
        super(AssetType.class, forVariable(variable));
    }

    public QAssetType(Path<? extends AssetType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAssetType(PathMetadata metadata) {
        super(AssetType.class, metadata);
    }

}

