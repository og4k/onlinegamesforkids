package com.onlinegamesforkids.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLanguageRu is a Querydsl query type for LanguageRu
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLanguageRu extends EntityPathBase<LanguageRu> {

    private static final long serialVersionUID = -2100089396L;

    public static final QLanguageRu languageRu = new QLanguageRu("languageRu");

    public final QAbstractAuditingEntity _super = new QAbstractAuditingEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.Instant> createdDate = _super.createdDate;

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final StringPath lastModifiedBy = _super.lastModifiedBy;

    //inherited
    public final DateTimePath<java.time.Instant> lastModifiedDate = _super.lastModifiedDate;

    public final StringPath name = createString("name");

    public final StringPath threeLetterName = createString("threeLetterName");

    public final StringPath twoLetterName = createString("twoLetterName");

    public QLanguageRu(String variable) {
        super(LanguageRu.class, forVariable(variable));
    }

    public QLanguageRu(Path<? extends LanguageRu> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLanguageRu(PathMetadata metadata) {
        super(LanguageRu.class, metadata);
    }

}

